$(document).ready(function(){
	$grid = $('#grid-content');
	
	$.fn.revealItems = function($items){
		
		var iso = this.data('isotope');
		var itemSelector = iso.options.itemSelector;
		$items.hide();
		$(this).append($items);
		return this;
	}
	$grid.isotope({
		containerStyle: null,
		masonry:{
			columnWidth: 300,
			gutter: 15
		},
		itemSelector: '.grid-item',
		filter : '*',
		transitionDuration: '0.4s'
	});

	
	$grid.imagesLoaded().progress(function(){
		$grid.isotope();
	})

	function GenerateItems(){
		var items = '';
			items = document.getElementById("grid-content").innerHTML;;
		
		return $(items);
	}
	
	$grid.revealItems(GenerateItems());
	
	$(document).on('click','.filter-item',function(){
		$('.filter-item.active').removeClass('active');
		$(this).addClass('active');
		var f = $(this).data('f');
		console.log(f);
		$grid.find('.grid-item').removeClass('wow').removeClass('fadeInUp');
		$grid.isotope({filter: f});
		
	})
	
	
	$(window).resize(function(){
		var margin=40;
		var padding=15;
		var columns=0;
		var cWidth=300;
		var windowWidth = $(window).width();

		var overflow = false;
		while(!overflow){
			columns++;
			var WidthTheory = ((cWidth*columns)+((columns+1)*padding)+margin);
			if(WidthTheory > windowWidth)
				overflow = true;			
		}		
		if(columns > 1)
			columns--;
		
		var GridWidth = ((cWidth*columns)+((columns+1)*padding)+margin);
		
		if( GridWidth != $('#grid').width()){
			$('#grid').width(GridWidth);
		}
	});
//	$(window).scroll(Infinite);
//	new WOW().init();

})