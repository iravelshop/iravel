<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
     
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <!-- Styles -->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}"/>     
   @laravelPWA
</head>

<style type="text/css">
 
 
  .flex.online-users.xs3 {
    display: none;
}
span.small.font-italic
{

  display: none;
}

.v-chip
{
  border-radius: 4px;
}
.v-chip span {
    float: right;
   
    text-align: right;
    width: auto;
    color: #fff;
   
    width: 80%;
    padding: 5px;
    border-radius: 5px;

}
div#privateMessageBox
{
  overflow-y: hidden;
}
.flex.caption.font-italic
{
  margin-left: 900px;
}

.red {
    background-color: #e8e8ea!important;
    border-color: #e8e8ea!important;
}
.message-wrapper {
    display: flex;
}
.text-message-container {
    width: 70%;
   
}
span.v-chip.green.white--text {
    float: right;
}
.flex.caption.font-italic {
    margin-left: 20px;
}


</style>     
<body class="chat-fullview">
  <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-sm-4 col-xs-3">
        <a class="back-btn pt-10 pb-10" href="/private" style="display: table; line-height: 36px;"><img src="/svg/1-a.svg"></a>
      </div>
       @if(!empty($actusers))
                 @foreach($actusers as $user)
      <div class="col-sm-4 col-xs-6"><h2 class="product-head">{{$user->name}}</h2></div>
      <div class="col-sm-4 col-xs-3 pt-10 pb-10 product-search chat-pic">
        @if(isset($user->avatar))
        <img src="{{ url('storage/'.$user->avatar) }}"  class="pull-right" />
      @endif
    </div>
      @endforeach
                @endif
    </div>
  </div>

    <div id="app">
        <main class="mt-5">
            <v-container fluid>
                @yield('content')
            </v-container>
        </main>
    </div>
  

</body>
<style type="text/css">
  .white--text .v-chip__content
{
  color:black!important;
}
.green .v-chip__content
{
  color:white!important;
}
@media only screen and (max-width: 600px) {
.flex.xs9 {
     flex-basis: 100%; 
    flex-grow: 0;
     max-width: 100%; 
}
}
.caption {
     font-size: 9px!important; 
}
.green {
    background-color: #007f3d!important;
    border-color: #007f3d!important;
}
.grey
{
  background-color:#ffffff!important;
   border-color: #ffffff!important;
}
.image-container img
{
  height:200px;
  width:200px;
}
.v-btn__content {
  background-color: white!important;
  color:black!important;
      width: 20px!important;
    font-size: 22px!important;
}
.v-btn--floating:not(.v-btn--depressed):not(.v-btn--flat) {
  display:none;}
  .file-uploads {
    display: none;
  }
  button.mt-3.ml-2.white--text.v-btn.v-btn--small.theme--dark.green {
    background-color: white !important;
    /* border-color: white !important; */
    padding: 0;
    margin: 0;
    border: 0;
    box-shadow: none !important;
}
.flex.xs1 {
    flex-basis: 8.333333333333332%;
    flex-grow: 0;
    /* max-width: 8.333333333333332%; */
    max-width: 20px;
}
footer .layout
{
  border-color: #000;
    border-top-style: solid;
    border-top-style: thin;
    border-top-width: 0.01em;
}
</style>
</html>
