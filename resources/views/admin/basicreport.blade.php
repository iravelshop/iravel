

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Account and Profile page</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/css/custom.css"/>
  <link rel="stylesheet" type="text/css" href="/css/responsive.css"/>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
  
  <link rel="stylesheet" href="{{ asset('css/t-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/t-datepicker-main.css')}}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="{{ asset('js/t-datepicker.min.js')}}"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

</head>
<style type="text/css">
  .form-group label {
    color:black;
  }

</style>
<body class="product-fullview">

  <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-2">
        <a class="back-btn pt-10 pb-10" href="/home" style="display: table; line-height: 36px;"><img src="/svg/back-arrow.png"></a>
      </div>
      <div class="col-xs-6"><h2 class="product-head">Basic Report</h2></div>
     
    </div>
  </div>
 <form  method="get" enctype="multipart/form-data">


    <div class="container-fluid ">
      <br/>

      <div class="col-xs-2">
       
     
      <label for="Product Name">Product Category</label>
    </div>
     <div class="col-xs-4">

          <select class="form-control" name="category" id="category" class="form-control custom-field"       >
 <option value="0" selected>Select</option>
              @foreach($subcategory as $item)
                @if(isset($_GET['category']) and ($_GET['category'] ==$item->_id ))
                
                  <option value="{{ $item->_id }}" selected>{{ $item->name }}</option>
               @else
                 <option value="{{ $item->_id }}" >{{ $item->name }}</option>
               @endif
              @endforeach
            </select>
         
        </div>

            <div class="col-xs-2">
               <label for="Product Name">Location</label>
             </div>
              <div class="col-xs-4">
             <select class="form-control" name="location" id="location"  class="form-control custom-field"       >
<option value="0" selected>Select</option>
              @foreach($location as $item)
               
                 @if(isset($_GET['location']) and ($_GET['location'] ==$item ))
                  <option value="{{ $item }}" selected>{{ $item}}</option>
                @else
                <option value="{{ $item }}" >{{ $item}}</option>
              
                 @endif
               
              @endforeach
            </select>
          </div>
           <br/>
           <div class="col-xs-12">&nbsp;</div>
            <div class="col-xs-2">
               <label for="Product Name">Date</label></div>
            <div class="col-xs-9">
              
           <div class="t-datepicker">
  <div class="t-check-in"></div>
            <div class="t-check-out"></div>
          </div>

        </div>
       <div class="col-xs-4"></div>
         <div class="col-xs-4 text-center"> <button  class="btn-lg-custom publish-btn" id=search style="background-color:#8bc73e ;color:black" onclick="serch()">Search</button></div>
    </div>
    <br/>
    <div>
       @if(!empty($booking))
       <table width=98% border=1 cellpadding="20" cellspacing="5" style=";margin-left:10px;margin-right:10px" class=tblreport>
        <tr style="font-weight: bold"><td>Category<td>Product Name</td><td>Owner</td><td>Location</td><td>Rented By </td><td>Rented From </td><td>Rented To </td></tr>
       
      @foreach($booking as $book)
      <tr>
         @foreach($product as $prod)
                 @if ($book->product_id==$prod->_id)

                          @foreach($subcategory as $cat)
                           @if ($prod->category==$cat->_id)
                           <td>{{$cat->name}}</td>
                            @endif
                            @endforeach
                    <td>{{$prod->title}}</td><td>

                          @foreach($user as $users)
                     
                                    @if ($book->user_id==$users->_id)
                                  {{$users->name}}
                                    @endif
                          @endforeach
                    </td>
                    <td>{{$prod->city}}</td>
                 
                
                @endif
           @endforeach
           
          <td>
        @foreach($renter as $rent)
       
  @if ($book->user_id==$rent->_id)
{{$rent->name}}
  @endif
  @endforeach
  </td>
  <td>{{$book->start_dt}}</td><td>{{$book->end_dt}}</td></tr>
      @endforeach
    </table>
      @endif
    </div>
     @if(isset($_GET['tstart']) && $_GET['tstart']!='null') 
     <input type=hidden id=bookedstdt value={{$_GET['tstart']}} />
                 <input type=hidden id=bookedenddt value={{$_GET['tend']}} />
                 @endif
</form>

  <script type="text/javascript">
     // initialize manually with a list of links
  jQuery(function($){
   var disabledDays = [
        "8-22-2019", "22-8-2019", "20-8-2019"
    ];

          function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }


                   if ( document.getElementById('bookedstdt') != null)
                  {
                       if (document.getElementById('bookedstdt').value!="")
                        {
                              //document.getElementById('submitDeal').disabled=false;
                              $('.t-datepicker').tDatePicker({
                                dateFormat: 'DD, d MM yy',
                                           minDate:new Date(2009, 10 - 1, 25),
                                            titleDateRange: 'day',
                                            titleDateRanges: 'days',
                                             titleCheckIn: 'Start Date',
                                              titleCheckOut: 'End Date',
                                      dateCheckIn:document.getElementById('bookedstdt').value,
                                    dateCheckOut:document.getElementById('bookedenddt').value,
                                             numCalendar:1,
                                     
                                        });
               
                              }
                  }
                  else
                  {
                    availableDates = ['12-08-2019','13-08-2019','04-22-2015'];
                                     $('.t-datepicker').tDatePicker({
                                      dateFormat: 'DD, d MM yy',
                              minDate:new Date(2009, 10 - 1, 25),
                              titleDateRange: 'day',
                              titleDateRanges: 'days',
                               titleCheckIn: 'Start Date',
                                titleCheckOut: 'End Date',

                               numCalendar:1,
                               beforeShowDay: function(date){
                                var day = date.getDay();
                                 var string = jQuery.datepicker.formatDate('d-m-yy', date);
                                 var isDisabled = ($.inArray(string, disabledDays) != -1);

                                 //day != 0 disables all Sundays
                                 return [day != 0 && !isDisabled];
                            }
                          });
                  }
                   $('.t-datepicker').on('beforeShowDay',function(dt) {
     console.log('beforeShowDay do something'.dt)
  })
        
        var disableddates =['08-09-2019','08-19-2019','08-22-2019'];
 
        function DisableSpecificDates() {
         
         var m = date.getMonth();
         var d = date.getDate();
         var y = date.getFullYear();
         
         // First convert the date in to the mm-dd-yyyy format 
         // Take note that we will increment the month count by 1 
         var currentdate = (m + 1) + '-' + d + '-' + y ;
         

         
         // We will now check if the date belongs to disableddates array 
         for (var i = 0; i < disableddates.length; i++) {
         
         // Now check if the current date is in disabled dates array. 
         if ($.inArray(currentdate, disableddates) != -1 ) {
         return [false];
         } 
         }
         
         // In case the date is not present in disabled array, we will now check if it is a weekend. 
         // We will use the noWeekends function
         var weekenddate = $.datepicker.noWeekends(date);
         return weekenddate; 
         
        }
      }
        
  );
  function serch()
        {
        // alert();
 //window.location.href = "https://127.0.0.1:8000/report?loc="+document.getElementById("location").value+"&cat="+document.getElementById("category").value; 
        }
  </script>
  <style type="text/css">
    .tblreport
    {
      font-size: 18px;
    }
    @media screen and (max-width: 768px)
{
  .tblreport
    {
      font-size: 9px;
    }
    .publish-btn {
    width: 100%;
    }
}


  </style>
</body>
</html>