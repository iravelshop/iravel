
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Product Capture</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.min.css') }}"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

 
	


	


	 @laravelPWA
</head>





<body class="bg-dark">
<section class="product-desc-wrapper">
  <div class="container">
  <div class="wd-80-auto width-sm-100">
   <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-2">
        <a class="back-btn pt-10 pb-10" href="/home" ><img src="/svg/back-arrow.png"></a>
     </div>
     <div class="col-xs-10"><h2 class="product-desc-head">Tell Something about the product</h2></div>

    </div>
  </div>
  	
 
  <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data" id=frmcapture>

{{ csrf_field() }}
  <?php $intcol=1 ;?>
 @if(Session()->has('imgUrl') )
  @foreach(Session::get('imgUrl') as $item1)
 <img id=img{{$intcol}} src="{{$item1}}" />
 <?php $intcol++; ?>
  @endforeach
    @else
   @if(!empty($prodimage))
  @foreach($prodimage as $image)
 <img id=img{{$intcol}} src="{{ url('storage/'.$image->filename) }}" />
 <?php $intcol++; ?>
  @endforeach
    

  @endif
  
      @endif
@if(!empty($proddetails))
  @foreach($proddetails as $det)
  <div class="form-group">
        <label>Title</label>
         
      
        <input type="text" name="title" maxlength="50" placeholder="Enter Title" id="title" class="form-control custom-field" required value="{{$det->title}}"/>
        
      </div>
      <div class="form-group">
        <label>Price Per Day</label>
         
        <input type="number" min="0.01" step="0.01" name="price" id="price" placeholder="Enter Price" class="form-control custom-field" required value="{{$det->price}}"/>
       
      </div>
      <div class="form-group">
        <label>Description</label>
         
        <textarea name="description" id="description" class="form-control custom-field" maxlength="1000">{{$det->description}}</textarea>
       
      </div>
      <div class="form-group">
      <label for="Product Name">Product Sub category</label>
      <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Select Cateory 
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu2" id="dropdown-menu1">
       
            @foreach($subcategory as $item)
          @if ( $item->id == $det->category) 
           <a class="dropdown-item active" idval="{{$item->_id}}">{{ $item->name }}</a>
           @else
            <a class="dropdown-item " idval="{{$item->_id}}">{{$item->name }}</a>
           @endif
        @endforeach
          
        </div>
      </div>


          </div>
          <input type=hidden id=hdndraftid value="{{$det->_id}}" name=hdndraftid />
           <input type=hidden name=hdncat id=hdncat value="{{$det->category}}" />
  @endforeach
     @else
       <div class="form-group">
        <label>Title</label>
          @if(Session()->has('title'))
        <input type="text" name="title" maxlength="50" placeholder="Enter Title" id="title" class="form-control custom-field" value="{{Session("title")}}" required/>
        @else
        <input type="text" name="title" maxlength="50" placeholder="Enter Title" id="title" class="form-control custom-field" required/>
        @endif
      </div>
      <div class="form-group">
        <label>Price Per Day</label>
           @if(Session()->has('price'))
        <input type="number" min="0.01" step="0.01" name="price" id="price" placeholder="Enter Price" class="form-control custom-field" value={{Session("price")}} required/>
        @else
        <input type="number" min="0.01" step="0.01" name="price" id="price" placeholder="Enter Price" class="form-control custom-field" required/>
        @endif
      </div>
      <div class="form-group">
        <label>Description</label>
          @if(Session()->has('description'))
        <textarea name="description" id="description" class="form-control custom-field" maxlength="1000">{{Session("description")}}</textarea>   @else
        <textarea name="description" id="description" class="form-control custom-field" maxlength="1000"></textarea>
        @endif
      </div>
       <div class="form-group">
      <label for="Product Name">Product Sub category</label>
      <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Select Category 
  </button>
   @if(Session()->has('category') )
    <input type=hidden name=hdncat id=hdncat value="{{Session('category') }}" />
   @else
    <input type=hidden name=hdncat id=hdncat value="" />
   @endif
  <div class="dropdown-menu" aria-labelledby="dropdownMenu2" id="dropdown-menu1">
      
            @foreach($subcategory as $item)
          @if(Session()->has('category') )
                 @if ( $item->id ==Session('category') ) 
                 
           <a class="dropdown-item active" idval="{{$item->_id}}">{{ $item->name }}</a>
           @else
            <a class="dropdown-item " idval="{{$item->_id}}">{{$item->name }}</a>
           @endif
           @else
           
            <a class="dropdown-item " idval="{{$item->_id}}">{{$item->name }}</a>
           @endif
        @endforeach
          
        </div>
      </div>
</div>
      
 @if(Session()->has('draftid') )
            <input type=hidden id=hdndraftid value="{{Session('draftid')}}" name=hdndraftid />
            @else
             <input type=hidden id=hdndraftid value="0" name=hdndraftid />
            @endif
          @endif
            
           <div >

             <div class="text-center">
       
        </div>
      <div class="text-center">
        
        <button type="submit" class="btn-lg-custom publish-btn" id=btnCapture name="btnCapture">Capture</button>
        <button  class="btn-lg-custom publish-btn" id=save>Save</button>
      
        @if(isset($_GET['id']))
             <button type="submit" class="btn-lg-custom publish-btn" id=remove>Remove</button>
           
            @endif
          @if(isset($_GET['id']))
        <p class="pt-10 pb-10 text-center"><a class="white" href="{{url('/products/create?id='.$_GET['id'])}}">Cancel</a></p>
        @else
         <p class="pt-10 pb-10 text-center"><a class="white" href="/products/create">Cancel</a></p>
        @endif
      </div>
       <input type="hidden" id=hdnlat name=hdnlat value="{{Session::get('lat')}}">
           
               
            <input type="hidden" id=hdnlong name=hdnlong value="{{Session::get('long')}}">
      <input type=hidden name="noofsnaps" id="noofsnaps" value="0">
        <input type=hidden name="typesubmit" id="typesubmit" value="0">
       <input type=hidden name="hdnSnap" id="hdnSnap" value="">
        <input type=hidden name=hdncity id=hdncity>
    <input type=hidden name=hdnstate id=hdnstate>
     <input type=hidden name=hdnCountry id=hdnCountry>
    <input type=hidden name=hdntown id=hdntown>
     <input type=hidden name=hdnloc id=hdnloc>
     
    </form>
  </div>
</div>


</div>


</section>

	
</body>
<script type="text/javascript">
  $(document).ready(function(){
    if (document.getElementById('hdncat').value!="")
       $('#dropdownMenu2').text( $("#dropdown-menu1").find('.active').text());
     else
$('#dropdownMenu2').text( $("#dropdown-menu1 a:first-child").text());
  $('#dropdown-menu1 a').click(function () {
         
          $('#dropdownMenu2').text($(this).text());
        document.getElementById('hdncat').value=$(this).attr('idval');
        
    });
});
  window.addEventListener("DOMContentLoaded", function() {

      
           
      document.getElementById('btnCapture').addEventListener('click', function() {
       
        document.getElementById('typesubmit').value=1;

        
      });
   
      document.getElementById('save').addEventListener('click', function() {

        if (document.getElementById("hdnlat").value=="" )
        {
               if(navigator.geolocation) {
               //  alert("kkkk");
                         navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
                         //strCity=document.getElementById("hdncity").value;
                     } else {
                          alert("Geolocation is not supported by this browser.");
                        }
                         event.preventDefault();
                }
            
        

        
      });
      if (document.getElementById('remove'))
        {       document.getElementById('remove').addEventListener('click', function() {
             
              document.getElementById('typesubmit').value=2;

              
            });}
          }, false);
      
function geoSuccess(position) {
  
    document.getElementById('hdnlat').value= position.coords.latitude;
    document.getElementById('hdnlong').value = position.coords.longitude; 
    if ( position.coords.latitude!="")
    { $.getJSON('https://api.tiles.mapbox.com/v4/geocode/mapbox.places/'+position.coords.longitude+','+position.coords.latitude+'.json?access_token=pk.eyJ1IjoicmFteWFrIiwiYSI6ImNqdzF1cnAyODAyb28zenFyZTFvYjZ5aDEifQ.6_FhtdOUY4keozu6y5EeaQ', function(data) {
  
     parseReverseGeo(data.features[0]);
    

     });
  //return strcity;
  }
}

function geoError(error) {
   
   switch (error.code)
  {
    case error.PERMISSION_DENIED:
      // User denied access to location. Perhaps redirect to alternate content?
     alert('Permission was denied to track current location. Allow to track your location for better experience');
      break;
    case error.POSITION_UNAVAILABLE:
      alert('Position is currently unavailable.');
      break;
    case error.PERMISSION_DENIED_TIMEOUT:
      alert('User took to long to grant/deny permission.');
      break;
    case error.UNKNOWN_ERROR:
      alert('An unknown error occurred.')
      break;
    }
}
function parseReverseGeo(geoData) {
 
                    // debugger;
                    var state,city,town, countryName, placeName, returnStr;
                   town=geoData.text;
                    if(geoData.context){

                        $.each(geoData.context, function(i, v){
                            if(v.id.indexOf('region') >= 0) {
                                state = v.text;
                            }
                             if(v.id.indexOf('district') >= 0 || v.id.indexOf('place') >= 0) {
                                city = v.text;
                            }
                            if(v.id.indexOf('country') >= 0) {
                                countryName = v.text;
                            }
                        });
                    }

                    if(town && state && countryName) {


                      document.getElementById('hdntown').value=town;
                      document.getElementById('hdncity').value=city;
                      document.getElementById('hdnstate').value=state;
                      document.getElementById('hdnCountry').value=countryName;
                     // return city;
                       // returnStr = town + ','+ city + ","+state + "," + countryName;
                    } else {
                      document.getElementById('hdnloc').value=geoData.place_name;
                     // return geoData.place_name;
                        
                    }
                    document.getElementById('typesubmit').value=0;
                   // alert();
                    frmcapture.submit();
                   // return '';
                   
                }
</script>
<style type="text/css">
  #style
  {
    -webkit-appearance:none !important;
  }
  
  .btn-secondary:hover{
     color:#007f3d; 
     background-color: #ffffff; 
    border-color: #ffffff; 
}
      li.dropdown.dropdown-notifications {
    /* width: 18%; */
    float: right;
    display: inline-block;
    position: relative;
  }
  .dropdown-notifications>.dropdown-container, .dropdown-notifications>.dropdown-menu {
    width: 300px;
    max-width: 300px;
}
#dropdownMenu2
{
  background-color: white;
  color:black;
  width:300px;
  text-align: left;
}
.dropdown-menu
{
  width:300px;
}
.dropdown-toggle::after {
  text-align: right;
    float: right;
    margin-top: 8px;
  }
 .dropdown {
    margin-top: 5px;
}
 .w3-container p
{
  margin-top:15px;
  margin-bottom: 12px;
  font-size: 18px;
  color:red;
}
.w3-container
{
  background-color: white;
 
  font-color:red;
  margin-top:15px;
}
span.w3-button.w3-red.w3-large.w3-display-topright {
    float: right;
}
.w3-display-container
{
  height: 50px;
}
</style>
</html>
	