<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Product Capture</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.min.css') }}"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

   @laravelPWA

  <style>
  .wd-80-auto,.overlay,.videodiv{
    text-align: center;}
  video
  {
    width: 55%;
  }
  @media (max-width: 1000px) {

     video
  {
    width: 98%;
  }

}

    .cancelbtn a
    {
      font-size: 18px;
    }
canvas{
   
  margin-left: 10px;margin-right:10px;
}
.btn-remove
{
border-radius: 50%;
height:25px;
width:25px;
display: inline-block;
background-image: url("/svg/cancel.png");background-position:center;
background-repeat:no-repeat;
position:absolute;
    margin-left: -35px;
 
}
.btnCapture {
  
    width:50px;
    height:50px;
    line-height:50px;
    border: 2px solid #f5f5f5;
    border-radius: 50%;
    color:#f5f5f5;
    text-align:center;
    text-decoration:none;
    background: #464646;
    box-shadow: 0 0 3px gray;
    font-size:20px;
    font-weight:bold;
}
.btnCapture:hover {
    background: #262626;
}
@media screen and (max-width: 661px){
  .col-xs-3,.col-xs-4
  {
    padding-right: 0px;
    padding-left: 0px;
  }
  .container-fluid>.navbar-collapse, .container-fluid>.navbar-header, .container>.navbar-collapse, .container>.navbar-header
  {
    margin-left: 10px;
  }
}
</style>
</head>




<body class="bg-dark">
  <form  action="{{ route('capturestore') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<section class="product-desc-wrapper">
  <div class="container">
  <div class="wd-80-auto width-sm-100">
   <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-4 ">
       <input type=button value="Cancel" id="cancel" class="btn-lg-custom publish-btn" onclick="window.location='{{ url("products/create") }}'">  
     </div>
      <div class="col-xs-3">
       
          <input id="video" type="file" onchange="readURL(this);" name="upload" accept=”image/jpeg,image/png"   style="display: none"/>
         <input type=button  id="media" class="btn-lg-custom publish-btn" style='background: url("/svg/camara-icon.png") no-repeat scroll 0 0 transparent;padding-bottom: 20px'> </div>

     <div class="col-xs-4"><input type=submit value="Publish" id="publish" class="btn-lg-custom publish-btn" >  </div>

    </div>
  </div>
  
<?php $intcol=0;?>
   @if(Session()->has('imgUrl') )
  @foreach(Session::get('imgUrl') as $item1)
 <img id=img{{$intcol}} src={{$item1}} style="display: none;" />
 <?php $intcol++; ?>


  @endforeach
   @else
    @if(!empty($prodimage))
      @foreach($prodimage as $image)
     <img id=img{{$intcol}} src="{{ url('storage/'.$image->filename) }}" style="display: none;"/>
     <?php $intcol++; ?>


      @endforeach
    @endif
     @endif

  </div>
  <div class=videodiv>
  <!--<video id="video"  autoplay playsinline></video>-->
   
</div>
 
  <div class=imgwrap id=divCanvas>
  
      </div>
      </div>
      </section>
        <input type=hidden name="hdnSnap" id="hdnSnap" value="">
        <input type=hidden name="hdnleft" id="hdnleft" value="10">
    </form>
      </body>
<script>
  $(function(){
      $("#media").on('click', function(e){
          e.preventDefault();
            
          $("#video:hidden").trigger('click');
           
            
           
      });
    });

    function readURL(input) {
     
        
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
              //event.preventDefault(); 
        // Grab elements, create settings, etc.
        intcount=0;

        intcount=document.getElementsByTagName("canvas").length;
        if (intcount<6)
        {
           var canv = document.createElement("canvas");
                      canv.setAttribute('width', 150);
                      canv.setAttribute('height', 120);
                canv.setAttribute('id', "canvas");
                document.getElementById("divCanvas").appendChild(canv);
          var btn = document.createElement("button");
                      btn.setAttribute('width', 150);
                      btn.setAttribute('height', 120);
                btn.setAttribute('id', "btnremove");
                btn.setAttribute('class', "btn-remove");
                btn.setAttribute('onclick', "remove(this)");
              //  btn.setAttribute('style', "left:"+document.getElementById('hdnleft').value+"%");
              document.getElementById("divCanvas").appendChild(btn);
               var canvas = canv;
              var context = canvas.getContext('2d');
              var img = new Image();
img.onload = function(){

    canv.width = 150;
    canv.height = 120;

    context.drawImage(this, 0, 0,150,120);

  //  alert(myCanvas.toDataURL('image/jpeg'))

   // console.log(myCanvas.toDataURL('image/jpeg'));
};

img.src = e.target.result;
                
                 /* var canvas = canv;
                    
                      
                      var context = canvas.getContext('2d');
                  context.drawImage(e.target.result, 0, 0,150, 120);*/
                }
                else
                  alert("Allowed only 6 images per product.")

            
              
                   
            };

           
        }
          reader.readAsDataURL(input.files[0]);
    }
    // Put event listeners into place
    window.addEventListener("DOMContentLoaded", function() {
      
//$('#video').click();
 
  intcount=document.getElementsByTagName("img").length;
 
      for (i=0;i<intcount;i++)
       {
          
          img =  new Image();
          
            img.src =document.getElementById("img"+i).src;
            img.id = i;
                  //context.drawImage(javascript_array[i], 0, 0,150, 120);
                  
                  
                     canvas  = document.createElement('canvas');
                    $(canvas).attr('id', i);
                    canvas.height=120;
                    canvas.width=150;
                    var context = canvas.getContext('2d');
                    document.getElementById("divCanvas").appendChild(canvas);
                //      context.drawImage(img, 0, 0);
                      var btn = document.createElement("button");
                      btn.setAttribute('width', 150);
                      btn.setAttribute('height', 120);
                btn.setAttribute('id', "btnremove");
                btn.setAttribute('class', "btn-remove");
                btn.setAttribute('onclick', "remove(this)");
              //  btn.setAttribute('style', "left:"+document.getElementById('hdnleft').value+"%");
                document.getElementById("divCanvas").appendChild(btn);
              img.onload = function() {
                    //Use the image id to get the correct context
                    var canvas = document.getElementById(this.id);
                    var context = canvas.getContext('2d');
                    context.drawImage(this, 0, 0);
                } 
                  
                  
                  
        }
            var video = document.getElementById('video');
            //var mediaConfig =  { video: true };
var front = false;

      // Trigger photo take
      document.getElementById('video').addEventListener('onchange', function() {
        event.preventDefault(); 
        // Grab elements, create settings, etc.
        intcount=0;

        intcount=document.getElementsByTagName("canvas").length;
        if (intcount<6)
        {
           var canv = document.createElement("canvas");
                      canv.setAttribute('width', 150);
                      canv.setAttribute('height', 120);
                canv.setAttribute('id', "canvas");
                document.getElementById("divCanvas").appendChild(canv);
          var btn = document.createElement("button");
                      btn.setAttribute('width', 150);
                      btn.setAttribute('height', 120);
                btn.setAttribute('id', "btnremove");
                btn.setAttribute('class', "btn-remove");
                btn.setAttribute('onclick', "remove(this)");
              //  btn.setAttribute('style', "left:"+document.getElementById('hdnleft').value+"%");
                document.getElementById("divCanvas").appendChild(btn);
                  var canvas = canv;
                    
                      
                      var context = canvas.getContext('2d');
                  context.drawImage(video, 0, 0,150, 120);
                }
                else
                  alert("Allowed only 6 images per product.")

          
      });
      document.getElementById('publish').addEventListener('click', function() {
        
        intcount=document.getElementsByTagName("canvas").length;
        
        if (intcount==0)
          event.preventDefault(); 
        for (i=0;i<intcount;i++)
        document.getElementById('hdnSnap').value=document.getElementById('hdnSnap').value+"@@"+document.getElementsByTagName("canvas")[i].toDataURL();



        
      });
      
    }, false);
function remove(e)
{
  event.preventDefault(); 
  e.parentNode.removeChild(e.previousSibling);
  e.parentNode.removeChild(e);
  
}
  </script>
