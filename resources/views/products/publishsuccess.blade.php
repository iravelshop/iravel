
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Publish Success </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.min.css') }}"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

 
	


	


	 @laravelPWA
</head>

<style type="text/css">
  .publishsuccess {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 370px;
}
.parasuccess
{
  
  height: 24px;
  font-family: Lato;
  font-size: 20px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.2;
  letter-spacing: 0.29px;
  text-align: center;
  color: #282c40;
  text-align: center;
}
.proddesc
{

  height: 19px;
  font-family: Lato;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.19;
  letter-spacing: 0.23px;
  text-align: center;
  color: #007f3d;
  width: 100%;
}
</style>



<body >
  <div class="container">
<div class="publishsuccess">
  <img src="/svg/success.png" >
</div>
	<p class=parasuccess>Product Successfully Published</p>
   @if(!empty($proddet))
          @foreach($proddet as $prod)
  <label class="proddesc">{{$prod->title}}</label>
  @endforeach
  @endif
  @if(!empty($prodimages))
  @foreach($prodimages as $image)
 <img id=img src="{{ url('storage/'.$image->filename) }}" />

  @endforeach
    

  @endif
  <br/>
 <div class="col-xs-12" style="text-align:center"><a href="/home">Home</a></div>
</div>
</body>

 
</style>
</html>
	