
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Product publish</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.min.css') }}"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	
 @laravelPWA
	<style>
		video { border: 1px solid #ccc; display: block; margin: 0 0 20px 0; }
		#canvas { margin-top: 20px; border: 1px solid #ccc; display: block; }
   .form-control:disabled, .form-control[readonly] {
     background-color: #000000; 
    opacity: 1;
}
 #description:disabled {
     background-color: #ffffff; 
    opacity: 1;
}
  
	</style>
</head>



<body class="bg-dark">
   @if ($message = Session::get('paypalverify'))
        <div class="w3-panel w3-green w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-red w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        @endif
<section class="product-desc-wrapper">
<div class="container">
  <div class="wd-80-auto width-sm-100">
   <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-2">
        <a class="back-btn pt-10 pb-10" href="/home" ><img src="/svg/back-arrow.png"></a>
     </div>
     <div class="col-xs-10"><h2 class="product-desc-head">Tell Something about the product</h2></div>

    </div>
  </div>
  	
<form  action="{{ route('publishstore') }}" method="post" enctype="multipart/form-data">
   @if ($message = Session::get('paypalverify'))
     <div class="form-group">
                          <label>PayPal Email Id</label>
                           @if (Session::has('paypalid'))
                          <input type="text" name="paypal" placeholder="Enter PayPal Email Id" id="paypal" class="form-control custom-field" value="{{Session::get('paypalid')}}" />
                          <?php Session::forget('paypalid'); ?>
                          @else
                           <input type="text" name="paypal" placeholder="Enter PayPal Email Id" id="paypal" class="form-control custom-field" />
                          @endif
                        </div>
                       <?php Session::forget('paypalverify'); ?>
   @endif

{{ csrf_field() }}
   @if(!empty($prodimage))
    <div class="product-imgs-wrap">
      <div class="product-imgs">
      <?php $intval = 0; ?>
         @foreach($prodimage as $image)
             

        <img src="{{ url('storage/'.$image->filename) }}">
       
      
                  <input type=hidden name=hdnid value="{{$image->products_id}}" />
                  <?php $intval++; ?>  
                @endforeach
                </div>
    </div>
               @endif
               @if(!empty($proddetails))
  @foreach($proddetails as $det)
       


       <div class="form-group">
        <label>Title</label>
        <input type="text" name="title"  value="{{$det->title}}"" id="title" class="form-control custom-field"  />
      </div>
      <div class="form-group">
        <label>Price Per Day</label>
        <input type="text" name="price" id="price"  value="{{$det->price}}" class="form-control custom-field"  />
      </div>
      <div class="form-group">
        <label>Description</label>
        <textarea name="description" id="description" class="form-control custom-field"  >{{$det->description}}</textarea>
      </div>
      <div class="form-group">
      <label for="Product Name">Product Sub category</label>

      <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
    Select Category 
  </button>
<div class="dropdown-menu" aria-labelledby="dropdownMenu2" id="dropdown-menu1">
      

            @foreach($subcategory as $item)
          @if($det->category==$item->id)
                 
           <a class="dropdown-item active" idval="{{$item->_id}}">{{ $item->name }}</a>
           @else
            <a class="dropdown-item " idval="{{$item->_id}}">{{$item->name }}</a>
          
           @endif
        @endforeach
          </div>
        </div>
        </div>
      <div class="text-center">
             <input type=hidden name="hdnid" id="hdnid" value="{{$det->_id}}">
             <input type=hidden name=hdncat id=hdncat value="{{$det->category}}" />
                @if(!isset($det->product_id))
              <input type=hidden name="hdnproductid" id="hdnproductid" value="0">
              @else
              <input type=hidden name="hdnproductid" id="hdnproductid" value="{{$det->product_id}}">
              
              @endif
      @endforeach
@endif
        <button type="submit" class="btn-lg-custom publish-btn" id="publish">Publish</button>
        <p class="pt-10 pb-10 text-center"><a class="white" HREF="javascript:history.go(0)">Cancel</a></p>
      </div>
    
       <input type=hidden name="hdnSnap" id="hdnSnap" value="">
    </form>
  </div>
</div>




</section>

	
</body>
<style type="text/css">
   .btn-secondary:hover{
     color:#007f3d; 
     background-color: #ffffff; 
    border-color: #ffffff; 
}
      li.dropdown.dropdown-notifications {
    /* width: 18%; */
    float: right;
    display: inline-block;
    position: relative;
  }
  .dropdown-notifications>.dropdown-container, .dropdown-notifications>.dropdown-menu {
    width: 300px;
    max-width: 300px;
}
#dropdownMenu2
{
  background-color: white;
  color:black;
  width:300px;
  text-align: left;
}
.dropdown-menu
{
  width:300px;
}
.dropdown-toggle::after {
  text-align: right;
    float: right;
    margin-top: 8px;
  }
.w3-container
{
  background-color: white;
 
  font-color:red;
  margin-top:15px;
}
span.w3-button.w3-red.w3-large.w3-display-topright {
    float: right;
    padding-right: 5px;
}
.w3-display-container
{
  height: 50px;
}
.w3-panel.w3-green.w3-display-container {
    background: palevioletred;
    text-align: center;
}
.w3-panel.w3-green.w3-display-container p{
    line-height: 30px;
}
</style>
<script>
  $(document).ready(function(){
    if (document.getElementById('hdncat').value!="")
       $('#dropdownMenu2').text( $("#dropdown-menu1").find('.active').text());
     else
$('#dropdownMenu2').text( $("#dropdown-menu1 a:first-child").text());
  $('#dropdown-menu1 a').click(function () {
         
          $('#dropdownMenu2').text($(this).text());
        document.getElementById('hdncat').value=$(this).attr('idval');
        
    });
});
  </script>
</html>
	