<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Search</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/custom.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/responsive.min.css"/>
 
   @laravelPWA
</head>
<style type="text/css">
	.search-ctrl
	{
		width:250px;
  height: 30px;
  opacity: 0.5;
  font-family: Lato;
  font-size: 20px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.2;
  letter-spacing: 0.29px;
  text-align: left;
  color: #282c40;
  margin-left: 10px;
  border:0px;
	}
	body {
    min-width: 100%;
    width: 100%;
    max-width: 100%;
    min-height: 100% !important;
    height: 100% !important;
    max-height: 100% !important;
}
</style>
<body class="product-fullview-search">
	<form id ="frmhome"  method="GET">
  <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-4">
      	  @if (! isset($_GET['text']))
        <a class="back-btn pt-10 pb-10" href="/home" style="display: table; line-height: 36px;"><img src="/svg/1-a.svg"></a>
        @else
        <a class="back-btn pt-10 pb-10" href="/search" style="display: table; line-height: 36px;"><img src="/svg/1-a.svg"></a>
       
        @endif
      </div>
      <div class="col-xs-4"><h2 class="product-head">Search</h2></div>
      
    </div>
  </div>
  <div class="container-fluid searchdiv" style="margin-top:15px">
 
        
         <a  id="hrefsearch"> <img src="/svg/21.svg"></a>
         @if (! isset($_GET['text']))

        <input type="text" id=txtSearch name=txtSearch placeholder="What are you looking for ?" class="search-ctrl"/>
        @else
         <input type="text" id=txtSearch name=txtSearch placeholder="What are you looking for ?" class="search-ctrl" value="{{$_GET['text']}}"/>
      
        @endif
       
@if (! isset($_GET['text']))
<div class="col-xs-12" style="height:100px;" >&nbsp;</div>
          <div id="emptydiv"  class="wd-80-auto" style="text-align:center;">
    <img src="/svg/Search.svg"
     class="Empty" />
     
     <br/>
 </div>
 @endif
 <div id="SlideMiddle">
 <div id="grid">
         
          <div id="grid-content">
            <?php $intcol=3 ;?>
              @if(!empty($products))
             @foreach($products as $prod)
              @if ($intcol%3==0)
                      <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))
                                    @foreach($products_image as $image)
                                         @if ($image->products_id == $prod->_id)
                                       @if ($prod->user_id!=Auth::user()->id)
                                        <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                          <?php
                        // Example 1
                        $desc  = $prod->description;
                        $arrdesc = explode(" ", $desc);
                        $str='';
                        $strprev='';
                         for ($x = 0; $x < count($arrdesc); $x++) {
                          if (strlen($str.' '.$arrdesc[$x])<21)
                            $str=$str.' '.$arrdesc[$x];
                            else
                              break;

                         } 
                        
                        if (strlen($desc)<21)
                        echo  '<label class="product-title">'.$str.'</label>'; // piece1
                      else
                         echo  '<label class="product-title">'.$str.'.. </label>'; // piece1
                        

                        ?>
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">  @if(!empty($prod->currency)) 
                            {{$prod->currency}}
                            @else
                           {{'$'}}
                            @endif  <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @elseif  ($intcol%3==1)
                  <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))
                                    @foreach($products_image as $image)
                                         @if ($image->products_id == $prod->_id)
                                         @if ($prod->user_id!=Auth::user()->id)
                                        <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                         <?php
                        // Example 1
                        $desc  = $prod->description;
                        $arrdesc = explode(" ", $desc);
                        $str='';
                        $strprev='';
                         for ($x = 0; $x < count($arrdesc); $x++) {
                          if (strlen($str.' '.$arrdesc[$x])<21)
                            $str=$str.' '.$arrdesc[$x];
                            else
                              break;

                         } 
                        if (strlen($desc)<21)
                        echo  '<label class="product-title">'.$str.'</label>'; // piece1
                      else
                         echo  '<label class="product-title">'.$str.'.. </label>'; // piece1
                        ?>
                          
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">
                            @if(!empty($prod->currency)) 
                            {{$prod->currency}}
                            @else
                           {{'$'}}
                            @endif 
                            <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @elseif  ($intcol%3==2)
                    <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))

                                    @foreach($products_image as $image)
                                   
                                         @if ($image->products_id == $prod->_id)

                                         @if ($prod->user_id!=Auth::user()->id)
                                        <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                          <?php
                        // Example 1
                        $desc  = $prod->description;
                        $arrdesc = explode(" ", $desc);
                        $str='';
                        $strprev='';
                         for ($x = 0; $x < count($arrdesc); $x++) {
                          if (strlen($str.' '.$arrdesc[$x])<21)
                            $str=$str.' '.$arrdesc[$x];
                            else
                              break;

                         } 
                        
                        if (strlen($desc)<21)
                        echo  '<label class="product-title">'.$str.'</label>'; // piece1
                      else
                         echo  '<label class="product-title">'.$str.'.. </label>'; // piece1
                        ?>
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">  @if(!empty($prod->currency)) 
                            {{$prod->currency}}
                            @else
                           {{'$'}}
                            @endif <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @endif
             @endforeach
             @endif
          </div>
      </div>
  </div>
</div>
</form>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 

</body>
<script type="text/javascript">
	$(document).ready(function(){
 
  $('#hrefsearch').click(function () {
  	previous=getUrlParameter('text');
           str=self.location.toString(); 
              if (str.indexOf('text') > -1)
                self.location=str.replace(previous,$('#txtSearch').val());
            else
            	self.location= self.location+"?text="+$('#txtSearch').val();
  	// window.location.href = "?text=" + $('#txtSearch').val();
  });
  $(document).on('keyup keypress', 'form input[id="txtSearch"]', function(e) {

  if(e.which == 13) {
    
  previous=getUrlParameter('text');
           str=self.location.toString(); 
              if (str.indexOf('text') > -1)
                self.location=str.replace(previous,$('#txtSearch').val());
            else
            	self.location= self.location+"?text="+$('#txtSearch').val();
  }
});
}
);
	var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
</script>
 
 

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.0/isotope.pkgd.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js'></script>
<script  src="js/index.js"></script>
     
      
     
    </div>
</body>
</html>

