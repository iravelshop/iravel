@extends('layouts.app')
<style type="text/css">
  .chat-list.row
  {
    margin-left: 0px;
  }
</style>
<body class="chatroom" >
  <div class="container-fluid bg-dark" >
    <div class="navbar-header " >
      <div class="col-xs-4">
        <a class="back-btn pt-10 pb-10" href="/home" style="display: table; line-height: 36px;"><img src="/svg/1-a.svg"></a>
      </div>
      <div class="col-xs-4"><h2 class="product-head">Chat</h2></div>
    </div>
  </div>
<div class="container-fluid"  >
   @if(!empty($messages))
                 @foreach($messages as $message)
   
           
               
                  <?php 
                  $msgpresent=0;
                  ?>
               @if(!empty($users))
     @foreach($users as $user)
                     @if (($message->user_id == $user->_id) or ($message->receiver_id == $user->_id))
                      <div class="chat-list row">
              <div class="chat-list-item active-chat">
                <div class="chat-pic col-sm-2 col-xs-2"><a href="privatechat?userid={{$user->_id}}">
                  @if (isset($user->avatar))
                  <img src="{{ url('storage/'.$user->avatar) }}" />
                  @else
                  <img src="svg/Avatar.png" />
                @endif
              </a></div>
                 <div class="chat-profile-name col-md-7 col-xs-7"><a href="privatechat?userid={{$user->_id}}">{{$user->name}}</a> 
                      <?php  
                      $datetime1 = new DateTime();
                    $datetime2 = new DateTime($message->created_at);
                    $diff = $datetime1->diff($datetime2);
                    $diff->w = floor($diff->d / 7);
                    $msgpresent=1;
                        $diff->d -= $diff->w * 7;
                        $string = array(
                            'y' => 'year',
                            'm' => 'month',
                            'w' => 'week',
                            'd' => 'day',
                            'h' => 'hour',
                            'i' => 'minute',
                            's' => 'second',
                        );
                        foreach ($string as $k => &$v)
                        {
                            if ($diff->$k)
                            {
                                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                            }
                            else
                            {
                                unset($string[$k]);
                         }
                        }
                        $string = array_slice($string, 0, 1);
                        $intmessage= $string ? implode(', ', $string) . ' ago' : 'just now';

                     ?>
                   <span>{{$message->message}}</span></div>
                   
                      
                      <div class="chat-time col-xs-3">{{$intmessage}}</div>
 </div>
</div>
            
                       @endif
                   @endforeach
                     @endif
                     
             

     @endforeach
           @endif
           
   @if(!empty($userrest))
     @foreach($userrest as $user)

            <div class="chat-list row">
              <div class="chat-list-item active-chat">
                <div class="chat-pic col-sm-2 col-xs-2"><a href="privatechat?userid={{$user->_id}}">
                  @if (isset($user->avatar))
                  <img src="{{ url('storage/'.$user->avatar) }}" />
                  @else
                  <img src="svg/Avatar.png" />
                @endif
              </a></div>
                 <div class="chat-profile-name col-md-7 col-xs-7"><a href="privatechat?userid={{$user->_id}}">{{$user->name}}</a> 
                  <?php 
                  $msgpresent=0;
                  ?>
                
              </div>
            </div>
</div>
     @endforeach
           @endif
  


  
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</body>
