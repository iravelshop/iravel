<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title></title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!-- Styles -->

<style>

.container {

margin-top:2%;

}

</style>

</head>

<body>

@if (count($errors) > 0)

<div class="alert alert-danger">

<ul>

@foreach ($errors->all() as $error)

<li>{{ $error }}</li>

@endforeach

</ul>

</div>

@endif

<div class="container">

<div class="row">

<div class="col-md-2"></div>

<div class="col-md-8"><h2>SubCategory capture</h2>

</div>

</div>

<br>

<div class="row">

<div class="col-md-3"></div>

<div class="col-md-6">

<form action="{{ route('subcategory.store') }}" method="post" enctype="multipart/form-data">

{{ csrf_field() }}

<div class="form-group">

<label for="Product Name">SubCategory Name</label>

<input type="text" name="name" class="form-control"  placeholder="Product Name" >

</div>
<div class="form-group">

<label for="Product Name">SubCategory Description</label>

<input type="text" name="description" class="form-control"  placeholder="Product description" >

</div>
Category
 <select class="form-control" name="category">
              @foreach($category as $item)
              <option value="{{ $item->id }}">{{ $item->name }}</option>
              @endforeach
            </select>
<input type="file" class="form-control" name="catphoto"  />

<button type="submit" class="btn btn-primary">Submit</button>


</form>

</div>

</div>

</div>

</body>

</html>