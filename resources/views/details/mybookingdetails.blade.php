<!doctype html>

<html lang="{{ app()->getLocale() }}">



<head>
  <title>Iravel - Booking Details</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
  <link rel="stylesheet" type="text/css" href="css/custom.min.css">
  <link rel="stylesheet" type="text/css" href="css/responsive.min.css">
  <link rel="stylesheet" href="css/t-datepicker.min.css">
<link rel="stylesheet" href="css/t-datepicker-main.css">


@laravelPWA
<style>
td {
    padding-left: 5px;
}
.fade
{
  opacity:1;
}
.container {

margin-top:2%;

}
* {box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

#inputs,
#errors,
#directions {
    position: absolute;
    width: 33.3333%;
    max-width: 300px;
    min-width: 200px;
}

#inputs {
    z-index: 10;
    top: 10px;
    left: 10px;
}

#directions {
    z-index: 99;
    background: rgba(0,0,0,.8);
    top: 0;
    right: 0;
    bottom: 0;
    overflow: auto;
}

#errors {
    z-index: 8;
    opacity: 0;
    padding: 10px;
    border-radius: 0 0 3px 3px;
    background: rgba(0,0,0,.25);
    top: 90px;
    left: 10px;
}
#map
{
  height:300px;
  width:48%;
  margin-right:15px;
  margin-bottom:10px;
  margin-top:0px;
}
.product-location img
{
  height: 40px;
  width:40px;
}
p.product-location {
    display: inline;
}
@media screen and (max-width: 661px){
#map
{
  height:300px;
  width:90%;
  margin-right:15px;
  margin-bottom:10px;
  margin-top:0px;
  margin-left: 10px
}
.carousel-inner>.item>img
{
  width: 100%;
  height:auto;
}
.contact2
{
  margin-top:15px;
  margin-left: 10px;
  margin-right: 10px;
}
}
.col-sm-6.col-xs-5.contact1 a
{
  color:white;
}

.carousel-inner
{
  text-align: center;
    padding-left: 10px;
    background-color: white;
    padding-right: 10px;
    padding-top: 10px;
  
}
.trheading
{
  font-size: 20px;
  font-weight:20px;
}
@media screen and (min-width: 800px) {
.carousel-inner>.item>img
{
  width: 100%;
  height:600px;
}
}
.carousel
{
  background-color: white;
}

@media screen and (max-width: 661px){
.prdimg
{
  width: 100%;
  height:auto !important;
}
td
{
  font-size: 10px;
}
.trheading
{
  font-size: 10px;
  font-weight:20px;
}

}
.pricetag {
   
    font-size: 32px;
    color: #178b3d;
    font-weight: 600;
}
.booking
{
 width: 100%;
}
.booking table
{
 width: 100%;
 
}
.bookingimage
{
  text-align: center;
}
.bookingimage img
{
  height: 350px;
  width:400px;
  }
@media screen and (max-width: 768px)
{
.product-fullview .interest-btn {
  padding-bottom: 12px;
  text-align: right;
  }
  .bookingimage img
  {
    height: 250px;
    width:300px;
    }
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://raw.githubusercontent.com/kartik-v/bootstrap-star-rating/master/css/star-rating.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


</head>



<body class="product-fullview">
  <form  method="post" action="{!! URL::to('feedstore') !!}" enctype="multipart/form-data" id=frmSubmit>

{{ csrf_field() }}

   <div class="w3-container">
         @if ($message = Session::get('success'))
        <div class="w3-panel w3-green w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-green w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
       <?php Session::forget('success');?>
        @endif

        @if ($message = Session::get('error'))
        <div class="w3-panel w3-red w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-red w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('error');?>
        @endif

      
    </div>
   <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-2">
        <a class="back-btn pt-10 pb-10" href="/home" style="display: table; line-height: 36px;"><img src="/svg/back-arrow.png"></a>
      </div>
      <div class="col-xs-8"><h2 class="product-head">My Booking</h2></div>
      
    </div>
  </div>
   <div class="profle-view">
    <div class="container-fluid">
    <!-- Wrapper for slides -->
   <div class=bookingimage>
        @if(!empty($prodimage))
     
         @foreach($prodimage as $image)
             
               
                  <img src="{{ url('storage/'.$image->filename) }}"    class="prdimg" />
               
                  <input type=hidden name=hdnid value="{{$image->products_id}}" />
                 
                @endforeach
               @endif
      
    </div>
 
 
   @if(!empty($proddetails))
  @foreach($proddetails as $det)
        
 
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <div class="profile-desc">
          <h3>{{$det->title}}</h3>
          <input type=hidden name=hdntitle id=hdntitle value="{{$det->title}}">
          <p>{{$det->description}}.</p>
      
</div>
<span class="pricetag"> 
@if(!empty($det->currency)) 
                            {{$det->currency}}
                             <input type=hidden name=hdncurrency id=hdncurrency value="{{$det->currency}}" >
                            @else
                            <input type=hidden name=hdncurrency id=hdncurrency value="$" >
                           {{'$'}}
                            @endif <label id="price">{{$det->price}} </label>
         
                           </span>
                          
                          Per day</div>
                       

                         
 <div class="col-sm-6 col-xs-12">
       
      </div>
       @endforeach
@endif
<div class=booking>
  <br/>
  <br/>
  <table border=1 cellpadding="10" BORDERCOLOR="#A9A9A9">
    <tr class="trheading">
     
        <td>Start Date
        </td>
        <td>End Date
        </td>
        <td>No. Of Days
        </td>
        <td>Total Amount
        </td>
        <td>Payment 
        </td>
    </tr>
       @if(!empty($booking))
  @foreach($booking as $book)
  <tr>
   
        <td>{{$book->start_dt}}
        </td>
        <td>{{$book->end_dt}}
        </td>
        <td>{{$book->noofdays}}
        </td>
        <td>{{$book->total}}
        </td>
         <td>Success
        </td>

    </tr>
    <input type=hidden name=hdnprodid name=hdnprodid value="{{$book->product_id}}">
  @endforeach
  @endif
  </table>
  </div>
</div>

 </div>


</div>
 @if(!empty($review) && $review->count()>0)
 @if ($message = Session::get('Feedsave'))
        <div class="w3-panel  w3-display-container" style="text-align: center">
            
            <p style="color:red;text-align: center"><h4 style="color:red">{!! $message !!} !</h4></p>
        </div>
       <?php Session::forget('Feedsave');?>
        @endif

          @foreach($review as $rev)
<div class="feedback">
  <div class="col-xs-6"><h3>Feedback</h3></div>
   
       
  <div class="row11">
    <div class="col-xs-8">
      <div class="star-rating">
        <span class="fa fa-star-o" data-rating="1"></span>
        <span class="fa fa-star-o" data-rating="2"></span>
        <span class="fa fa-star-o" data-rating="3"></span>
        <span class="fa fa-star-o" data-rating="4"></span>
        <span class="fa fa-star-o" data-rating="5"></span>
        <input type="hidden" name="hdnRating" id="hdnRating" class="rating-value" value="{{$rev->rating}}">
      </div>
    </div>
  
  
 
</div>
<div class="col-xs-8">
<div class="form-group">
        <label>Comment</label>
         
        <textarea name="comment" id="comment" class="form-control custom-textarea" maxlength="1000">{{$rev->comment}}</textarea>
      </div>
      </div>
</div>


<div class="col-xs-6">
  <input type=hidden id=hdnreview value="{{$rev->_id}}" name=hdnreview>
  <input type=Submit value=Save id=save class="btn-lg-custom publish-btn">
</div>
@endforeach
@else
      <div class="feedback">
        <div class="col-xs-6"><h3>Feedback</h3></div>
         
             
        <div class="row11">
          <div class="col-xs-8">
            <div class="star-rating">
              <span class="fa fa-star-o" data-rating="1"></span>
              <span class="fa fa-star-o" data-rating="2"></span>
              <span class="fa fa-star-o" data-rating="3"></span>
              <span class="fa fa-star-o" data-rating="4"></span>
              <span class="fa fa-star-o" data-rating="5"></span>
              <input type="hidden" name="hdnRating" id="hdnRating" class="rating-value" value="">
            </div>
          </div>
        
        
       
      </div>
      <div class="col-xs-8">
      <div class="form-group">
              <label>Comment</label>
               
              <textarea name="comment" id="comment" class="form-control custom-textarea" maxlength="1000"></textarea>
            </div>
            </div>
      </div>


      <div class="col-xs-6">
        <input type=hidden id=hdnreview value="0" name=hdnreview>
        <input type=Submit value=Save id=save class="btn-lg-custom publish-btn">
      </div>
@endif
 @if(isset($_GET['bookingid']))
  <input type=hidden id=hdnbookid value={{$_GET['bookingid']}} name=hdnbookid>
  @endif
</form>
</body>
<style type="text/css">
  .w3-container p
{
  margin-top:15px;
  margin-bottom: 12px;
  font-size: 18px;
}
.star-rating {
  line-height:32px;
  font-size:1.25em;
}

.star-rating .fa-star{color: green;}
</style>
<script type="text/javascript">
  var $star_rating = $('.star-rating .fa');

var SetRatingStar = function() {
  return $star_rating.each(function() {
    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
      return $(this).removeClass('fa-star-o').addClass('fa-star');
    } else {
      return $(this).removeClass('fa-star').addClass('fa-star-o');
    }
  });
};

$star_rating.on('click', function() {
  $star_rating.siblings('input.rating-value').val($(this).data('rating'));
  return SetRatingStar();
});

SetRatingStar();
$(document).ready(function() {

});
</script>
</html>