<!doctype html>

<html lang="{{ app()->getLocale() }}">



<head>
  <title>Iravel - Booking Details</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
  <link rel="stylesheet" type="text/css" href="css/custom.min.css">
  <link rel="stylesheet" type="text/css" href="css/responsive.min.css">
  <link rel="stylesheet" href="css/t-datepicker.min.css">
<link rel="stylesheet" href="css/t-datepicker-main.css">


@laravelPWA
<style>
td {
    padding-left: 5px;
}
.fade
{
  opacity:1;
}
.container {

margin-top:2%;

}
* {box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

#inputs,
#errors,
#directions {
    position: absolute;
    width: 33.3333%;
    max-width: 300px;
    min-width: 200px;
}

#inputs {
    z-index: 10;
    top: 10px;
    left: 10px;
}

#directions {
    z-index: 99;
    background: rgba(0,0,0,.8);
    top: 0;
    right: 0;
    bottom: 0;
    overflow: auto;
}

#errors {
    z-index: 8;
    opacity: 0;
    padding: 10px;
    border-radius: 0 0 3px 3px;
    background: rgba(0,0,0,.25);
    top: 90px;
    left: 10px;
}
#map
{
  height:300px;
  width:48%;
  margin-right:15px;
  margin-bottom:10px;
  margin-top:0px;
}
.product-location img
{
  height: 40px;
  width:40px;
}
p.product-location {
    display: inline;
}
@media screen and (max-width: 661px){
#map
{
  height:300px;
  width:90%;
  margin-right:15px;
  margin-bottom:10px;
  margin-top:0px;
  margin-left: 10px
}
.carousel-inner>.item>img
{
  width: 100%;
  height:auto;
}
.contact2
{
  margin-top:15px;
  margin-left: 10px;
  margin-right: 10px;
}
}
.col-sm-6.col-xs-5.contact1 a
{
  color:white;
}

.carousel-inner
{
  text-align: center;
    padding-left: 10px;
    background-color: white;
    padding-right: 10px;
    padding-top: 10px;
  
}
.trheading
{
  font-size: 20px;
  font-weight:20px;
}
@media screen and (min-width: 800px) {
.carousel-inner>.item>img
{
  width: 100%;
  height:600px;
}
}
.carousel
{
  background-color: white;
}

@media screen and (max-width: 661px){
.prdimg
{
  width: 100%;
  height:auto !important;
}
td
{
  font-size: 10px;
}
.trheading
{
  font-size: 10px;
  font-weight:20px;
}

}
.pricetag {
   
    font-size: 32px;
    color: #178b3d;
    font-weight: 600;
}
.booking
{
 width: 100%;
}
.booking table
{
 width: 100%;
 
}
.bookingimage img
{
  height: 350px;
  width:400px;
  }
@media screen and (max-width: 768px)
{
.product-fullview .interest-btn {
  padding-bottom: 12px;
  text-align: right;
  }
  .bookingimage img
{
  height: 250px;
  width:300px;
  }
}
.bookingimage
{
  text-align: center;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>



<body class="product-fullview">
  <form  method="post" enctype="multipart/form-data" id=frmSubmit>

{{ csrf_field() }}

  <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-2">
        <a class="back-btn pt-10 pb-10" href="/home" style="display: table; line-height: 36px;"><img src="/svg/back-arrow.png"></a>
      </div>

      <div class="col-xs-8"><h2 class="product-head">Booking Details</h2></div>
    
    </div>
  </div>

  
  <div class="profle-view">
    <div class="container-fluid">
      <div class=bookingimage>
        @if(!empty($prodimage))
     
         @foreach($prodimage as $image)
             
               
                  <img src="{{ url('storage/'.$image->filename) }}"    class="prdimg" />
               
                  <input type=hidden name=hdnid value="{{$image->products_id}}" />
                 
                @endforeach
               @endif
      
    </div>
 
   @if(!empty($proddetails))
  @foreach($proddetails as $det)
        
 
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <div class="profile-desc">
          <h3>{{$det->title}}</h3>
          <input type=hidden name=hdntitle id=hdntitle value="{{$det->title}}">
          <p>{{$det->description}}.</p>
      
</div>
<span class="pricetag"> 
@if(!empty($det->currency)) 
                            {{$det->currency}}
                             <input type=hidden name=hdncurrency id=hdncurrency value="{{$det->currency}}" >
                            @else
                            <input type=hidden name=hdncurrency id=hdncurrency value="$" >
                           {{'$'}}
                            @endif <label id="price">{{$det->price}} </label>
         
                           </span>
                          
                          Per day</div>
                       

                         
 <div class="col-sm-6 col-xs-12">
        <div class="interest-btn"><a class="btn-lg-custom e" href="/products/create?id={{$det->draft_id}}" id="btninterest">Edit Details</a></div>
      </div>
       @endforeach
@endif
<div class=booking>
  <br/>
  <br/>
  <table border=1 cellpadding="10" BORDERCOLOR="#A9A9A9">
    <tr class="trheading">
      <td padding="5px">Rentee Name
        </td>
        <td>Rentee Email
        </td>
        <td>Rentee Phone
        </td>
        <td>Start Date
        </td>
        <td>End Date
        </td>
        <td>No. Of Days
        </td>
        <td>Total Amount
        </td>
    </tr>
       @if(!empty($booking))
  @foreach($booking as $book)
  <tr>
    @if(!empty($rentee))
  @foreach($rentee as $rent)
  @if ($book->user_id==$rent->_id)
      <td>{{$rent->name}}
        </td>
        <td>{{$rent->email}}
        </td>
        <td>{{$rent->phone}}
        </td>
        @endif
      @endforeach
  @endif   
        <td>{{$book->start_dt}}
        </td>
        <td>{{$book->end_dt}}
        </td>
        <td>{{$book->noofdays}}
        </td>
        <td>{{$book->total}}
        </td>
    </tr>
  @endforeach
  @endif
  </table>
  </div>
</div>




</div>
</div>
</form>
</body>

</html>