<!doctype html>

<html lang="{{ app()->getLocale() }}">



<head>
  <title>Iravel - Booking Details</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
  <link rel="stylesheet" type="text/css" href="css/custom.min.css">
  <link rel="stylesheet" type="text/css" href="css/responsive.min.css">
  <link rel="stylesheet" href="css/t-datepicker.min.css">
<link rel="stylesheet" href="css/t-datepicker-main.css">


@laravelPWA

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://raw.githubusercontent.com/kartik-v/bootstrap-star-rating/master/css/star-rating.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


</head>

<style type="text/css">
.ratingheader {
    display: flex;
}
.bookingimage 
  {
    text-align: center;
    }
  .bookingimage img
  {
    height:60px;
    width: 60px;
    margin-right:15px;
  }
  .ratingbody
  {
    margin:20px 20px 20px 20px;
  }
  .ratingheader img
  {
    height: 25px;
    width: 25px;
  }
  .ratingheader h2
  {
    font-family: Lato;
  font-size: 16px;
  
  font-style: normal;
  font-stretch: normal;
  line-height: 1.19;
  letter-spacing: 0.23px;
  text-align: left;
  color: #282c40;
  padding-left: 15px;
  margin-top: 2px;
  }
  .feed h3
  {
    font-family: Lato;
  font-size: 16px;
  
  font-style: normal;
  font-stretch: normal;
  line-height: 1.19;
  letter-spacing: 0.23px;
  text-align: left;
  color: #282c40;
 
  margin-top: 2px;
  }
  .ratingimage {
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
}
  .ratingimage img
  {
    width: 250.5px;
  height: 179.9px;
  object-fit: contain;
  }
  .imagerate
  {
  font-size: 11px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.8;
  letter-spacing: 0.14px;
  text-align: left;
  color: #37434d;
  margin-bottom: 0px;
}
.ownerdiv 
  {
    text-align: center;
    }
    .imageowner
    {
      font-family: Lato;
  font-size: 16px;
  
  font-style: normal;
  font-stretch: normal;
  line-height: 1.19;
  letter-spacing: 0.23px;
  text-align: left;
  color: #282c40;
  padding-left: 15px;
  margin-top: 2px;
  margin-bottom: 10px;
  font-weight: normal;
    }
    .profiledesc
    {
      text-align: center;
      margin-top: 15px;
    }
    .profiledesc label
    {
      font-family: Lato;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.19;
  letter-spacing: 0.23px;
  text-align: left;
  color: #007f3d;

    }
    .star-rating
    {
      text-align: center;
    }
    .star-rating .fa
    {
      color:#ffa733;
      font-size:22px;
    }
.feed-control input
{
 outline: 0;
    border-width: 0 0 1px;
    border-color: black;
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 0px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    margin-bottom: 20px;
}
.col-xs-12 
{
  text-align: center;
}
 .bookdate
{
  font-family: Lato;
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: 0.2px;
 
  color: #282c40;
      padding-left: 40px;
    opacity: 0.5;
}
</style>

<body class="product-fullview">
  <div class="ratingbody">
  <form  method="post" action="{!! URL::to('feedstore') !!}" enctype="multipart/form-data" id=frmSubmit>

{{ csrf_field() }}

        <div class="ratingheader">
            <a href="/home"><img src="/svg/path-889.svg"/></a>
            <h2>Rate your latest used item</h2>
          
                    
        </div>
          <div class="bookdate">
             @if(!empty($booking))
              @foreach($booking as $book)
                  <?php   $date=date_create($book->start_dt);
                      $date1=date_create($book->end_dt);

                       
                        echo date_format($date,"M d").' - '.date_format($date1,"M d");
                        ?>
                         
                        @endforeach
                       @endif
                     </div>
        <div class=ratingimage>
          <img src="/svg/rating.svg" />
        </div>
         <div class=ownerdiv>
           <label class="imagerate">Rate your product from</label><br/>
            @if(!empty($rentee))
     
         @foreach($rentee as $rentowner)
      <label class="imageowner">{{$rentowner->name}}</label>
        @endforeach
               @endif
    </div>
    <div class="bookingimage">
        @if(!empty($prodimage))
     
         @foreach($prodimage as $image)
             
               
                  <img src="{{ url('storage/'.$image->filename) }}"    class="prdimg" />
               
                  <input type=hidden name=hdnid value="{{$image->products_id}}" />
                 
                @endforeach
               @endif
     
    </div>
 
   <div class="proddesc">
    
    <!-- Wrapper for slides -->
  
 
         @if(!empty($proddetails))
        @foreach($proddetails as $det)
              <div class="profiledesc">
                <label>{{$det->title}}</label>
              </div>
        @endforeach
      @endif


</div>






 @if ($message = Session::get('Feedsave'))
        <div class="w3-panel  w3-display-container" style="text-align: center">
            
            <p style="color:red;text-align: center"><h4 style="color:red">{!! $message !!} !</h4></p>
        </div>
       <?php Session::forget('Feedsave');?>
        @endif

       

      <div class="feedback">
        
         
             
        <div class="row11">
         
            <div class="star-rating">
              <span class="fa fa-star-o" data-rating="1"></span>
              <span class="fa fa-star-o" data-rating="2"></span>
              <span class="fa fa-star-o" data-rating="3"></span>
              <span class="fa fa-star-o" data-rating="4"></span>
              <span class="fa fa-star-o" data-rating="5"></span>
              <input type="hidden" name="hdnRating" id="hdnRating" class="rating-value" value="">
            
          </div>
        
        
       
      </div>
      <div class="feed"><h3>Feedback</h3></div>
      <div class="feed-control">
     <input type=text id="comment" name="comment" class="formcntrl">
             
              </div>
     


      <div class="col-xs-12">
        <input type=hidden id=hdnreview value="0" name=hdnreview>
        <input type=Submit value=Done id=save class="btn-lg-custom publish-btn">
         <p class="pt-10 pb-10 text-center"><a class="" href="/home">Cancel</a></p>
      </div>

 @if(isset($_GET['bookingid']))
  <input type=hidden id=hdnbookid value={{$_GET['bookingid']}} name=hdnbookid>
  @endif

</form>
</div>
</body>

<script type="text/javascript">
  var $star_rating = $('.star-rating .fa');

var SetRatingStar = function() {
  return $star_rating.each(function() {
    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
      return $(this).removeClass('fa-star-o').addClass('fa-star');
    } else {
      return $(this).removeClass('fa-star').addClass('fa-star-o');
    }
  });
};

$star_rating.on('click', function() {
  $star_rating.siblings('input.rating-value').val($(this).data('rating'));
  return SetRatingStar();
});

SetRatingStar();
$(document).ready(function() {

});
</script>
</html>