<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>

<head>
  <title>Iravel - Book A Product</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
  <link rel="stylesheet" type="text/css" href="css/custom.min.css">
  <link rel="stylesheet" type="text/css" href="css/responsive.min.css">
  <link rel="stylesheet" href="css/t-datepicker.min.css">
<link rel="stylesheet" href="css/t-datepicker-main.css">


@laravelPWA
<style>
.fade
{
  opacity:1;
}
.container {

margin-top:2%;

}
* {box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

#inputs,
#errors,
#directions {
    position: absolute;
    width: 33.3333%;
    max-width: 300px;
    min-width: 200px;
}

#inputs {
    z-index: 10;
    top: 10px;
    left: 10px;
}

#directions {
    z-index: 99;
    background: rgba(0,0,0,.8);
    top: 0;
    right: 0;
    bottom: 0;
    overflow: auto;
}

#errors {
    z-index: 8;
    opacity: 0;
    padding: 10px;
    border-radius: 0 0 3px 3px;
    background: rgba(0,0,0,.25);
    top: 90px;
    left: 10px;
}
#map
{
  height:300px;
  width:48%;
  margin-right:15px;
  margin-bottom:10px;
  margin-top:0px;
}
.product-location img
{
  height: 40px;
  width:40px;
}
p.product-location {
    display: inline;
}
@media screen and (max-width: 661px){
#map
{
  height:300px;
  width:90%;
  margin-right:15px;
  margin-bottom:10px;
  margin-top:0px;
  margin-left: 10px
}
.carousel-inner>.item>img
{
  width: 100%;
  height:auto;
}
.contact2
{
  margin-top:15px;
  margin-left: 10px;
  margin-right: 10px;
}
}
.col-sm-6.col-xs-5.contact1 a
{
  color:white;
}

.carousel-inner
{
  text-align: center;
    padding-left: 10px;
    background-color: white;
    padding-right: 10px;
    padding-top: 10px;
  
}

</style>
<script src="https://api.tiles.mapbox.com/mapbox-gl-js/v0.37.0/mapbox-gl.js"></script>
<link href="https://api.tiles.mapbox.com/mapbox-gl-js/v0.37.0/mapbox-gl.css" rel="stylesheet"/>
</head>



<body class="product-fullview">
  <form action="{!! URL::to('paypal') !!}" method="post" enctype="multipart/form-data" id=frmSubmit>

{{ csrf_field() }}
<div class="bg-dark">
  <div class="container-fluid">
    <div class="navbar-header ">
      <div class="col-xs-4">
        <a class="back-btn pt-10 pb-10" style="display: table; line-height: 36px;" id="backbutton"><img src="/svg/back-arrow.png"></a>
      </div>
      <div class="col-xs-4"><h2 class="product-head">Product</h2></div>
    
    </div>
  </div>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
       @if(!empty($prodimage))
      <?php $intval = 0; ?>
         @foreach($prodimage as $image)
          @if($intval==0)
             <li data-target="#myCarousel" data-slide-to="{{$intval}}" class="active"></li>
          @else
              <li data-target="#myCarousel" data-slide-to="{{$intval}}" ></li>
          @endif
        <?php $intval++; ?>  
         @endforeach
      @endif
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        @if(!empty($prodimage))
      <?php $intval = 0; ?>
         @foreach($prodimage as $image)
              @if($intval==0)
                <div class="item active">
                  <img src="{{ url('storage/'.$image->filename) }}"    class="prdimg" />
                </div>
                @else
                    <div class="item ">
                    <img src="{{ url('storage/'.$image->filename) }}" class="prdimg" />
                  </div>
                 @endif
                  <input type=hidden name=hdnid value="{{$image->products_id}}" />
                  <?php $intval++; ?>  
                @endforeach
               @endif
      
    </div>
  </div>
  <div class="profle-view">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            @if(!empty($user))
          @foreach($user as $usr)
            @if (isset($usr->avatar))
              <div class="profile-pic"><img src="{{ url('storage/'.$usr->avatar) }}" id="imgowner"></div>
              @endif
              <div class="profile-name">
                <p><label id=ownerlbl>{{$usr->name}} </label>&nbsp;&nbsp;
                  
          @if(!empty($follow) && $follow->count()>0)
          <a href="/follow?user={{$usr->_id}}&id={{$_GET['id']}}&follow=n" class="btn-lg-custom e"  id="btnfollow" style="display: none">Un Follow</a>
@else
<a href="/follow?user={{$usr->_id}}&id={{$_GET['id']}}&follow=y" class="btn-lg-custom e"  id="btnfollow" style="display: none" >Follow</a>
@endif
</p>
                <input type="hidden" id=ownerlat value="{{$usr->lat}}">
                  <input type="hidden" id=ownerid name=ownerid value="{{$usr->_id}}">
                <input type="hidden" id=ownerlong value="{{$usr->long}}">
                  
              </div>
          @endforeach
           @endif

            <input type="hidden" id=userlat value={{Session::get('lat')}}>
           
               
            <input type="hidden" id=userlong value={{Session::get('long')}}>
           
        </div>
  @if(!empty($proddetails))
  @foreach($proddetails as $det)
        <div class="col-sm-6 col-xs-12">
          <div class="price-product"><span>
             @if(!empty($det->currency)) 
                            {{$det->currency}}
                             <input type=hidden name=hdncurrency id=hdncurrency value="{{$det->currency}}" >
                            @else
                            <input type=hidden name=hdncurrency id=hdncurrency value="$" >
                           {{'$'}}
                            @endif <label id="price">{{$det->price}} </label>
         
                           @if(!empty($det->distance)) 
                          <input type=hidden name=hdndistance id=hdndistance value="{{$det->distance}}" >
                          @else
                           <input type=hidden name=hdndistance id=hdndistance value="miles" >
                            @endif 
                          
                          </span> Per day</div>
        </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <div class="profile-desc">
          <h3>{{$det->title}}</h3>
          <input type=hidden name=hdntitle id=hdntitle value="{{$det->title}}">
          <p>{{$det->description}}.</p>
      @endforeach
@endif
          <p class="product-location">
           <img src="/svg/map-icon.png" id="mapicon" style="display: none;"><span id="distance"></span>
          </p>
        </div>
      </div>
      <div class="col-sm-6 col-xs-12">
        <div class="interest-btn"><button class="btn-lg-custom e" onclick="showDiv()" id="btninterest">I'm Interested</button></div>
      </div>
    </div>
  </div>
  </div>
</div>  
<section id=deal>
 <div class="product-deal-info" style="display:none;" id="dealDiv">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <h3>How long you need this?</h3>
        <div class="charged-price">
         <div class="input-group input-daterange">
    <input type="text" class="form-control" value="2012-04-05">
    <div class="input-group-addon">to</div>
    <input type="text" class="form-control" value="2012-04-19">
</div>
  @if(!empty($booking1) && $booking1->count()>0)
          @foreach($booking1 as $book)
             
                <p><input type=hidden id=bookedstdt value={{$book->start_dt}} /><span></span></p>
                 <p><input type=hidden id=bookedenddt value={{$book->end_dt}} /></p>
              <span class="ordered-price"><label id=lblprice ><span id=bookincurrency></span> {{$book->total}}</label></span>
          <span class="order-tag"><label id=lbltotal>Total Amount for {{$book->noofdays}} Days</label></span>
          <input type=hidden name="bookingid" id="bookingid" value={{$book->_id}} />
           <input type=hidden name="hdnTotalPrice" id="hdnTotalPrice" value={{$book->total}}/>
          <input type=hidden name="hdnNoofdays" id=hdnNoofdays value={{$book->noofdays}}/>
          @endforeach
          @else
           <input type=hidden id=bookingid name="bookingid" value=0 />
           <span class="ordered-price"><label id=lblprice ></label></span>
          <span class="order-tag"><label id=lbltotal></label></span>
           <input type=hidden id=hdnTotalPrice name="hdnTotalPrice"/>
          <input type=hidden id=hdnNoofdays name=hdnNoofdays />
           @endif
         
        </div>
      </div>
      <div class="col-sm-6 col-xs-12">
        <div id="inlineDatepicker"></div>
      </div>
    </div>
  </div>
    <div class="contact-map">
      <div class="col-sm-6 col-xs-12 mapping1" id="map" style="height: 0px">
      
      </div>
      <div class="col-sm-6 col-xs-12 contact2">
        <div class="call-person">
        <h4>
          <div class="col-sm-8 durationdiv">
            <span id=duration></span>
          </div>
          <div class="col-sm-6 col-xs-7">
            
            <div class="profile1-img"><img  id="imgowner1"/></div>
            <div class="profile1-name">
              <p><label id=owner></label>
                @if ($ratingval>0)
                <span><img src="/svg/starrating1.png" style="width:25px" />{{$ratingval}}</span></p>
                 @endif
            </div>
          </div>
          <div class="col-sm-6 col-xs-5 contact1">
             <span >&nbsp;</span><br/>
             @if(!empty($user))

          @foreach($user as $usr)
         
             @if (isset($usr->phone))
             
            <a href="tel:{{$usr->phone}}" class="fa fa-phone"  ><img src="/svg/call.svg" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
            @endif
            @if (isset($usr->email))
            <a href="mailto:{{$usr->email}}" class="fa fa-envelope"><img src="/svg/message.svg" /></a>
            @endif
            @endforeach
            @endif

          </div>
        </div>

        
        <div class="interest-btn"><button type="button"  class="btn-lg-custom e btn-disabled" id="submitDeal" disabled>Make a deal!</button></div>
      </div>
    </div>
</div>
</section>
</div>  


        <input type=hidden name=hdncity id=hdncity>          
 <input type=hidden name=hdnstate id=hdnstate>
 <input type=hidden name=hdntown id=hdntown>          
 <input type=hidden name=hdnCountry id=hdnCountry>



</form>

</div>

</div>

</div>

</body>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script type="text/javascript">
  
 jQuery(function($){

   var ar = <?php echo json_encode($arydates) ?>;
$('.input-daterange input').each(function() {
    $(this).datepicker();
});
   
  
 if ( document.getElementById('bookedstdt') != null)
{
   if (document.getElementById('bookedstdt').value!=""  )
    {
      $("#bookincurrency").text(document.getElementById('hdncurrency').value);
document.getElementById('submitDeal').disabled=false;
     
      $( "#submitDeal" ).removeClass( "btn-disabled" );
    $('.t-datepicker').tDatePicker({
      formatDate: 'yyyy-mm-dd',
    	  dateDisabled:ar,
        titleDateRange: 'day',
        titleDateRanges: 'days',
         titleCheckIn: 'Start Date',
dateCheckIn:document.getElementById('bookedstdt').value,
          dateCheckOut:document.getElementById('bookedenddt').value,
         numCalendar:1,
  titleCheckOut: 'End Date',
    }).on('afterCheckOut',function(e, dataDate){
      noofdays=days(dataDate[0],dataDate[1]);
      
      lbltotal.innerText="Total Amount for "+noofdays+" Days";
        lblprice.innerText=document.getElementById('hdncurrency').value+Number.parseFloat(noofdays*price.innerText).toFixed(2);
        document.getElementById('hdnTotalPrice').value=noofdays*price.innerText;
        document.getElementById('hdnNoofdays').value=noofdays;
    });
     $('.t-datepicker').on('onChangeCI',function(e, changeDateCI) {
           	
             if (  document.getElementsByName("tend")[0].value==""  || document.getElementsByName("tend")[0].value=="null")
             {
             	document.getElementById('submitDeal').disabled=true;
     
     			 $( "#submitDeal" ).addClass( "btn-disabled" );
             }
             else
             {
             	enddt=new Date(document.getElementsByName("tend")[0].value);
             	if (changeDateCI>enddt.getTime())
             	{
             		document.getElementById('submitDeal').disabled=true;
     
     			 $( "#submitDeal" ).addClass( "btn-disabled" );
             	}
           	
             }
          });
  }
 }
 else
    {
        
         $('.t-datepicker').tDatePicker({
          formatDate: 'yyyy-mm-dd',
        dateDisabled:ar,
        titleDateRange: 'day',
        titleDateRanges: 'days',
         titleCheckIn: 'Start Date',
         numCalendar:1,
        titleCheckOut: 'End Date',
       
          }).on('afterCheckOut',function(e, dataDate){
            noofdays=days(dataDate[0],dataDate[1]);
           // alert();
             document.getElementById('submitDeal').disabled=false;
     
      $( "#submitDeal" ).removeClass( "btn-disabled" );
            lbltotal.innerText="Total Amount for "+noofdays+" Days";
              lblprice.innerText=document.getElementById('hdncurrency').value+Number.parseFloat(noofdays*price.innerText).toFixed(2);
               document.getElementById('hdnTotalPrice').value=noofdays*price.innerText;
             document.getElementById('hdnNoofdays').value=noofdays;
          });
           $('.t-datepicker').on('onChangeCI',function(e, changeDateCI) {
           	
             if (  document.getElementsByName("tend")[0].value==""  || document.getElementsByName("tend")[0].value=="null")
             {
             	document.getElementById('submitDeal').disabled=true;
     
     			 $( "#submitDeal" ).addClass( "btn-disabled" );
             }
             else
             {
             	enddt=new Date(document.getElementsByName("tend")[0].value);
             	if (changeDateCI>enddt.getTime())
             	{
             		document.getElementById('submitDeal').disabled=true;
     
     			 $( "#submitDeal" ).addClass( "btn-disabled" );
             	}
           	
             }
          });

           
      function highlightDays(date) {

    for (var i = 0; i < ar.length; i++) {
        if (date - ar[i] == 0) {
            return [true,'', 'TOOLTIP'];
        }
    }
    return [false];

}
         
    }
/*   $('.t-datepicker').tdatepicker({
 beforeShowDay: function(date){
    	 	alert();
    var day = date.getDay();
    var string = jQuery.datepicker.formatDate('d-m-yy', date);
    alert( ($.inArray(string, disabledDays)));
    var isDisabled = ($.inArray(string, disabledDays) != -1);
//day != 0 disables all Sundayscho
return [day != 0 && !isDisabled];

}
  });*/
 });
 $( document ).ready(function() {
  $('#backbutton').on('click',function(e){
  	if (document.getElementById('btninterest').style.display=="none")
  	{	$('#backbutton').attr("href","#");
  window.location.reload();
}
  	else
  		$('#backbutton').attr("href","/home");
  }
  );
        owner.innerText=ownerlbl.innerText;
            if ( document.getElementById('imgowner') != null)
          {
            imgowner1.src=imgowner.src;
          }
           if ( document.getElementById('bookedstdt') != null)
          {
            if (document.getElementById('bookedstdt').value!="")
              {
                document.getElementById("btninterest").innerText="Extend Rent Period";
                
              }
           } 

       $('#submitDeal').on('click',function(e){
        // your stuff

            if (document.getElementsByName("tstart")[0].value=="null" || document.getElementsByName("tend")[0].value=="null" || document.getElementsByName("tstart")[0].value=="" || document.getElementsByName("tend")[0].value=="" )
              {
              
               e.preventDefault();
               return false;
              }
              else
                frmSubmit.submit();
        }); 
    });
 function days(strt,end) {
    var date1 = new Date(strt);
     var   date2 = new Date(end);
      
       var timeDiff = Math.abs(date2.getTime() - date1.getTime());
var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
return diffDays;
 //   console.log(diffDays); //show difference
}


</script>

<script>


function showDiv() {
 
        if (document.getElementById('userlat').value=="")
        {
          if(navigator.geolocation) {

                 navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
                
                 //strCity=document.getElementById("hdncity").value;
             } else {
                  alert("Please allow the application to track location by changing the settings in browser.");
                 // $('#map').hide();

                }
        }
        else
        	document.getElementById('map').style.height = "300px";

document.getElementById('btninterest').style.display = "none";
   document.getElementById('dealDiv').style.display = "block";
   event.preventDefault(); 
   document.getElementById('dealDiv').scrollIntoView();
   
  //var bounds = new mapboxgl.LngLatBounds();

//var boundbox = geojsonExtent(countryGeoJson);
       // map.fitBounds(boundbox,{padding: 20});
   
 
}
ownerlat=document.getElementById('ownerlat').value;
        ownerlong=document.getElementById('ownerlong').value;
        userlat=document.getElementById('userlat').value;
        userlong=document.getElementById('userlong').value;

         var resp;
        
        if (ownerlat!="" && userlat!="")
        {
          showMap();
          }
function showMap()
{
        mapboxgl.accessToken = 'pk.eyJ1IjoiYXBlcmN1IiwiYSI6ImNpZ3M0ZDZ2OTAwNzl2bmt1M2I0dW1keTIifQ.I5BD9uEHdm_91TiyBEk7Pw'
         ownerlat=document.getElementById('ownerlat').value;
        ownerlong=document.getElementById('ownerlong').value;
        userlat=document.getElementById('userlat').value;
        userlong=document.getElementById('userlong').value;

         var resp;
        
        if (ownerlat!="" && userlat!="")
        {
        var sw = new mapboxgl.LngLat(ownerlong, ownerlat);
        var ne = new mapboxgl.LngLat(userlong, userlat);
        var llb = new mapboxgl.LngLatBounds(sw, ne);
        const map = new mapboxgl.Map({
          container: 'map',
          style: 'mapbox://styles/mapbox/streets-v9',
           center: llb.getCenter(),
          zoom:8,
        })

        map.on('load', () => {
 
          var resp=$.get('https://api.mapbox.com/directions/v5/mapbox/driving/'+ownerlong+','+ownerlat+';'+userlong+','+userlat+'?access_token=pk.eyJ1IjoiYXBlcmN1IiwiYSI6ImNpZ3M0ZDZ2OTAwNzl2bmt1M2I0dW1keTIifQ.I5BD9uEHdm_91TiyBEk7Pw&geometries=geojson', 
            data => {
          
            map.addLayer({
              id: 'route',
              type: 'line',
              source: {
                type: 'geojson',
                data: {
                  type: 'Feature',
                  properties: {},
                  geometry: data.routes[0].geometry,
                },
              },
              layout: {
                'line-join': 'round',
                'line-cap': 'round',
              },
              paint: {
                'line-color': '#4286f4',
                'line-width': 6,
              },
            })
          
          })



        })
      var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
       //map._resizeCanvas(650,400);
       if (ownerlat!="" && userlat!="")
      {
       mapCanvas.style.width = '95%';
        map.resize();
      }
        // map._resizeCanvas(650,400);
        $.getJSON('https://api.mapbox.com/directions/v5/mapbox/driving/'+ownerlong+','+ownerlat+';'+userlong+','+userlat+'?access_token=pk.eyJ1IjoiYXBlcmN1IiwiYSI6ImNpZ3M0ZDZ2OTAwNzl2bmt1M2I0dW1keTIifQ.I5BD9uEHdm_91TiyBEk7Pw&geometries=geojson', function(data) {
            //data is the JSON string
            if (document.getElementById('hdndistance').value=="Kms")
           distance.innerText=Number.parseFloat(data.routes[0].distance/1000).toFixed(2) +" Kms away";
         else
           distance.innerText=Number.parseFloat((data.routes[0].distance/1000)*0.62137).toFixed(2) +" miles away";
        $('#mapicon').show();
           duration.innerText=Math.round(data.routes[0].duration/60) +" mins away";
           coordinates =data.routes[0].geometry.coordinates;
         //map.fitBounds([[ownerlong, ownerlat],[userlong,userlat]]);
        var bounds = coordinates.reduce(function(bounds, coord) {
        return bounds.extend(coord);
        }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
         
        map.fitBounds(bounds, {
        padding: 20
        });
         console.log(data.routes[0]);
        });
        const bounds = new mapboxgl.LngLatBounds();
        // map._resizeCanvas(650,400);
        //map.resize();
        //console.log(resp);
         }
}
function geoSuccess(position) {
 // console.log(position);
    document.getElementById('userlat').value= position.coords.latitude;
    document.getElementById('userlong').value = position.coords.longitude; 
     document.getElementById('map').style.height = "300px";
   //document.getElementById('userlat').value= "28.388491";
  // document.getElementById('userlong').value = "-97.791063"; 
   
    if ( position.coords.latitude!="")
    { $.getJSON('https://api.tiles.mapbox.com/v4/geocode/mapbox.places/'+position.coords.longitude+','+position.coords.latitude+'.json?access_token=pk.eyJ1IjoicmFteWFrIiwiYSI6ImNqdzF1cnAyODAyb28zenFyZTFvYjZ5aDEifQ.6_FhtdOUY4keozu6y5EeaQ', function(data) {
  
     parseReverseGeo(data.features[0]);
     

     });

   ownerlat=document.getElementById('ownerlat').value;
        ownerlong=document.getElementById('ownerlong').value;
        userlat=document.getElementById('userlat').value;
        userlong=document.getElementById('userlong').value;
      
 if (ownerlat!="" && userlat!="")
showMap();
  //return strcity;
  }
}

function geoError(error) {
   
   switch (error.code)
  {
    case error.PERMISSION_DENIED:
      // User denied access to location. Perhaps redirect to alternate content?
      alert('Please allow the application to track location by changing the settings in browser.');
      // $('#map').hide();
      break;
    case error.POSITION_UNAVAILABLE:
      alert('Position is currently unavailable.');
      break;
    case error.PERMISSION_DENIED_TIMEOUT:
      alert('User took to long to grant/deny permission.');
      break;
    case error.UNKNOWN_ERROR:
      alert('An unknown error occurred.')
      break;
    }
}
function parseReverseGeo(geoData) {
                    // debugger;
                    var state,city,town, countryName, placeName, returnStr;
                   town=geoData.text;
                    if(geoData.context){

                        $.each(geoData.context, function(i, v){
                            if(v.id.indexOf('region') >= 0) {
                                state = v.text;
                            }
                             if(v.id.indexOf('district') >= 0 || v.id.indexOf('place') >= 0) {
                                city = v.text;
                            }
                            if(v.id.indexOf('country') >= 0) {
                                countryName = v.text;
                            }
                        });
                    }

                    if(town && state && countryName) {

                      document.getElementById('hdntown').value=town;
                      document.getElementById('hdncity').value=city;
                      document.getElementById('hdnstate').value=state;
                      document.getElementById('hdnCountry').value=countryName;
                     // return city;
                       // returnStr = town + ','+ city + ","+state + "," + countryName;
                    } else {
                      document.getElementById('hdnloc').value=geoData.place_name;
                     // return geoData.place_name;
                        
                    }
                   // return '';
                   
                }
/*

var from = {
  "type": "Feature",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [80.2212091999999,12.975971]
  }
};
var to = {
  "type": "Feature",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [80.127457,12.922915]
  }
};
var options = {units: 'kilometers'};

var units = "kilometers";
var dista = turf.distance(from, to, units);
console.log(dista);*/
function Submit(id)
{
  //self.location=self.location+"?id="+id;
}
</script>
<style type="text/css">
  .btn-disabled {
  cursor: not-allowed !important;
  pointer-events: all !important;
}
button:disabled,
button[disabled]{
 
  color: #666666;
  opacity: 0.6;
}
@media screen and (min-width: 800px) {
.carousel-inner>.item>img
{
  width: 100%;
  height:600px;
}
}
.carousel
{
  background-color: white;
}

@media screen and (max-width: 661px){
.prdimg
{
  width: 100%;
  height:auto !important;
}
}
.durationdiv
{
  margin-bottom: 10px;
}
</style>
</html>
