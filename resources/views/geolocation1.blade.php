<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>Driving directions</title>
<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
<script src='https://api.mapbox.com/mapbox.js/v3.2.0/mapbox.js'></script>
<link href='https://api.mapbox.com/mapbox.js/v3.2.0/mapbox.css' rel='stylesheet' />
<style>
  body { margin:0; padding:0; }
  #map { position:absolute; top:0; bottom:0; width:100%; }
</style>
</head>
<body>
<style>
#inputs,
#errors,
#directions {
    position: absolute;
    width: 33.3333%;
    max-width: 300px;
    min-width: 200px;
}

#inputs {
    z-index: 10;
    top: 10px;
    left: 10px;
}

#directions {
    z-index: 99;
    background: rgba(0,0,0,.8);
    top: 0;
    right: 0;
    bottom: 0;
    overflow: auto;
}

#errors {
    z-index: 8;
    opacity: 0;
    padding: 10px;
    border-radius: 0 0 3px 3px;
    background: rgba(0,0,0,.25);
    top: 90px;
    left: 10px;
}

</style>

<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.3.0/mapbox-gl-geocoder.min.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.3.0/mapbox-gl-geocoder.css' type='text/css' />
<div id='map'></div>
<div id='inputs'></div>
<div id='errors'></div>
<div id='directions'>
  <div id='routes'></div>
  <div id='instructions'></div>
</div>
<p id=span></p>
<input type=hidden id="hdnLat">
<input type="hidden" id="hdnlong">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
    console.log(navigator.geolocation)
   /* $.getJSON('https://geoip-db.com/json/')
         .done (function(location) {
            $('#span').innerHTML= $('#span').innerHTML+(location.country_name);
             $('#span').innerHTML= $('#span').innerHTML+(location.state);
             $('#span').innerHTML= $('#span').innerHTML+(location.city);
            $('#span').innerHTML= $('#span').innerHTML+(location.latitude);
             $('#span').innerHTML= $('#span').innerHTML+(location.longitude);
             $('#span').innerHTML= $('#span').innerHTML+(location.IPv4);
         });*/
  } else {

    x.innerHTML = "Geolocation is not supported by this browser.";
  }


function showPosition(position) {
  document.getElementById("hdnlong").value=position.coords.latitude;
  document.getElementById("hdnLat").value=position.coords.longitude;
 alert(position.coords.latitude)
//  x.innerHTML = "Latitude: " + position.coords.latitude + 
  "<br>Longitude: " + position.coords.longitude; 
  $.getJSON('https://api.tiles.mapbox.com/v4/geocode/mapbox.places/'+ document.getElementById("hdnLat").value+','+document.getElementById("hdnlong").value+'.json?access_token=pk.eyJ1IjoicmFteWFrIiwiYSI6ImNqdzF1cnAyODAyb28zenFyZTFvYjZ5aDEifQ.6_FhtdOUY4keozu6y5EeaQ', function(data) {
    //data is the JSON string
   console.log(data.features[0]);
  span.innerHTML=parseReverseGeo(data.features[0]);

});
}
  



function parseReverseGeo(geoData) {
                    // debugger;
                    var state,city,town, countryName, placeName, returnStr;
                   town=geoData.text;
                    if(geoData.context){

                        $.each(geoData.context, function(i, v){
                            if(v.id.indexOf('region') >= 0) {
                                state = v.text;
                            }
                             if(v.id.indexOf('district') >= 0 || v.id.indexOf('place') >= 0) {
                                city = v.text;
                            }
                            if(v.id.indexOf('country') >= 0) {
                                countryName = v.text;
                            }
                        });
                    }

                    if(town && state && countryName) {
                        returnStr = town + ' , '+ city + " , "+state + ", " + countryName;
                    } else {
                        returnStr = geoData.place_name;
                    }
                    return returnStr;
                }
                var x = document.getElementById("demo");

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
    console.log(navigator.geolocation)
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }



</script>
</body>
</html>
