
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Account and Profile page</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv=“Pragma” content=”no-cache”>
<meta http-equiv=“Expires” content=”-1″>
<meta http-equiv=“CACHE-CONTROL” content=”NO-CACHE”>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/custom.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/responsive.min.css"/>
 @laravelPWA
</head>
<body class="product-fullview">
  <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-4">
        <a class="back-btn pt-10 pb-10" href="/profile" style="display: table; line-height: 36px;"><img src="/svg/1-a.svg"></a>
      </div>
      <div class="col-xs-4"><h2 class="product-head">Account</h2></div>
      
    </div>
  </div>
<div class="container-fluid">
    @if ($message = Session::get('paypalverify'))
        <div class="w3-panel w3-green w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-red w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <div class="edit-profile-wrapper">
                 <form action="{{ route('profileedit.store') }}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}
                   @if(!empty($users))
                     @foreach($users as $user)
                       
                          <div class="profile-img profile-acc-pic">
                           @if(!empty($user->avatar))
                          <img src="{{ url('storage/'.$user->avatar) }}" id="img" style="display: none;">
                        
                          @else
                            <img  id="profile" style="display: none;" >
                          @endif
                            <div class=videodiv>
  <!--<video id="video"  autoplay playsinline></video>-->
   
</div>
 
  <div class=imgwrap id=divCanvas>
  
      </div>
                        </div>
                          <input id="upload" type="file" onchange="readURL(this);" name="upload" accept=”image/jpeg,image/png"   /><br/>

             @if(!empty($user->avatar))
                          <a href="" id="upload_link">Change Photo</a>
                          @else
                            <a href="" id="upload_link">Upload Photo</a>
                          @endif
                       
                        <div class="form-group firstrow">
                          <label>Name</label>
                          <input type="text" name="uname" placeholder="Enter Username" id="uname" class="form-control custom-field" value="{{Session::get('name')}}" />
                        </div>
                        <div class="form-group">
                          <label>Your Age</label>
                          <input type="text" name="age" id="age" placeholder="Enter Age" class="form-control custom-field" value="{{Session::get('age')}}" />
                        </div>
                        <div class="form-group">
                          <label>Email Id</label>
                          <input type="text" name="email" placeholder="Enter Email" id="email" class="form-control custom-field" value="{{Session::get('email')}}" />
                        </div>
                         <div class="form-group">
                          <label>Phone</label>
                          <input type="text" name="phone" placeholder="Enter Phone" id="Phone" class="form-control custom-field" value="{{Session::get('phone')}}" />
                        </div>
                         <div class="form-group">
                          <label>PayPal Email Id</label>
                          <input type="text" name="paypal" placeholder="Enter PayPal Email Id" id="paypal" class="form-control custom-field" value="{{Session::get('paypalid')}}" />
                        </div>
                         <div class="form-group">
                          <label>Interested in </label>
                          <input type="text" name="interest" placeholder="Enter your Interested Topic" id="interest" class="form-control custom-field" value="{{Session::get('interest')}}" />
                        </div>
                  @endforeach
                  @endif
              <button class="btn-lg-custom">Update Profile</button>
              <input type=hidden id=hdnphoto >
              </form>
              
            </div>
            <?php Session::forget('paypalverify');
            Session::forget('name');
            Session::forget('age');
            Session::forget('email');
            Session::forget('phone');
            Session::forget('paypalid');
            Session::forget('interest');?>

       
    @else
              <div class="edit-profile-wrapper">
                 <form action="{{ route('profileedit.store') }}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}
                   @if(!empty($users))
                     @foreach($users as $user)
                       
                          <div class="profile-img profile-acc-pic">
                           @if(!empty($user->avatar))
                          <img src="{{ url('storage/'.$user->avatar) }}" id="profile" style="display: none;" >
                        
                          @else
                           <img  id="profile" style="display: none;" >
                          @endif
                           <div class=videodiv>
                        <!--<video id="video"  autoplay playsinline></video>-->
   
                              </div>
                               
                                <div class=imgwrap id=divCanvas>
                                
                                    </div>
                        </div>
                          <input id="upload" type="file" onchange="readURL(this);" name="upload" accept=”image/jpeg"   /><br/>

             @if(!empty($user->avatar))
                          <a href="" id="upload_link">Change Photo</a>
                          @else
                            <a href="" id="upload_link">Upload Photo</a>
                          @endif
                       
                        <div class="form-group firstrow">
                          <label>Name</label>
                          <input type="text" name="uname" placeholder="Enter Username" id="uname" class="form-control custom-field" value="{{$user->name}}" />
                        </div>
                        <div class="form-group">
                          <label>Your Age</label>
                          <input type="text" name="age" id="age" placeholder="Enter Age" class="form-control custom-field" value="{{$user->age}}" />
                        </div>
                        <div class="form-group">
                          <label>Email Id</label>
                          <input type="text" name="email" placeholder="Enter Email" id="email" class="form-control custom-field" value="{{$user->email}}" />
                        </div>
                         <div class="form-group">
                          <label>Phone</label>
                          <input type="text" name="phone" placeholder="Enter Phone" id="Phone" class="form-control custom-field" value="{{$user->phone}}" />
                        </div>
                         <div class="form-group">
                          <label>PayPal Email Id</label>
                          <input type="text" name="paypal" placeholder="Enter PayPal Email Id" id="paypal" class="form-control custom-field" value="{{$user->paypalid}}" />
                        </div>
                         <div class="form-group">
                          <label>Interested in </label>
                          <input type="text" name="interest" placeholder="Enter your Interested Topic" id="interest" class="form-control custom-field" value="{{$user->interest}}" />
                        </div>
                  @endforeach
                  @endif
    <button class="btn-lg-custom" id="update">Update Profile</button>
    <input type=hidden id=hdnphoto >
    <input type=hidden id=hdnSnap name="hdnSnap">
    </form>
    
  </div>
  @endif
</div>  


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <script type="text/javascript">
       $(function(){
      $("#upload_link").on('click', function(e){
          e.preventDefault();
            
          $("#upload:hidden").trigger('click');
           
            
           
      });
    });

    function readURL(input) {
     
        
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
              //alert();
              //event.preventDefault(); 
        // Grab elements, create settings, etc.
        intcount=0;
        intcount=document.getElementsByTagName("canvas").length;
img =  new Image();
          
            img.src =e.target.result;
            img.id = intcount;
            //img.src= e.target.result;
        

       
           var canv = document.getElementById("canvas1");
               if (!canv)   
               { canv=document.createElement("canvas");  
             canv.setAttribute('width', 100);
                      canv.setAttribute('height', 100);
                canv.setAttribute('id', "canvas1");
                document.getElementById("divCanvas").appendChild(canv);
               } 
                //canv.src=e.target.result;
                 var context = canv.getContext('2d');
                // $(canvas).attr('id', i);
                // var context = canv.getContext('2d');
              
                    
              //  btn.setAttribute('style', "left:"+document.getElementById('hdnleft').value+"%");
              
              img.onload = function() {
                    //Use the image id to get the correct context
                   // var canvas = document.getElementById(this.id);
               context.beginPath();
            context.arc(2*24, 2*24, 2*24, 0, Math.PI*2, true);
            context.clip();
            context.closePath();    
                   
    context.drawImage(img, 0, 0,100,100);
    
       
                } 
                //  var canvas = canv;
                    
                      
                    //  var context = canvas.getContext('2d');
                //  context.drawImage(e.target.result, 0, 0,150, 120);
              

            
              
                   
            };
  reader.readAsDataURL(input.files[0]);
        
           
        }
      //  document.getElementById("hdnphoto").value =document.getElementsByTagName("canvas")[0].toDataURL();
           
    }

     window.addEventListener("DOMContentLoaded", function() {
      
//$('#video').click();
 
  //intcount=document.getElementsByTagName("img").length;
 if (document.getElementById("profile").src!="")
 {
      
          var canv = document.createElement("canvas");
        //   canv.setAttribute('display', "none");
                      canv.setAttribute('width', 100);
                      canv.setAttribute('height', 100);
                canv.setAttribute('id', "canvas1");
                document.getElementById("divCanvas").appendChild(canv);
                var context = document.getElementById('canvas1').getContext("2d");

  var img = new Image();
img.onload = function () {
    context.beginPath();
context.arc(2*24, 2*24, 2*24, 0, Math.PI*2, true);
context.clip();
context.closePath();
//context.clip();
    context.drawImage(img, 0, 0,100,100);
   
           
    
     
}
img.src = document.getElementById("profile").src;
document.getElementById('hdnSnap').value=document.getElementsByTagName("canvas")[0].toDataURL();
}

 document.getElementById('update').addEventListener('click', function() {
        
        intcount=document.getElementsByTagName("canvas").length;
        
        if (intcount!=0)
        {
        
        document.getElementById('hdnSnap').value=document.getElementsByTagName("canvas")[0].toDataURL();

    }

//alert(document.getElementById('hdnSnap').value)

        
      });
    }, false);
  </script>
</body>
<style type="text/css">
  .edit-profile-wrapper .profile-img img {
    width:50%;
  }
 #upload_link
 {
    width: 104px;
  height: 19px;
  font-family: Lato;
  font-size: 16px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.19;
  letter-spacing: 0.23px;
  text-align: center;
  color: #007f3d;
 }
 .form-group.firstrow
 {
margin-top:15px;
 }
 .w3-container p
{
  margin-top:15px;
  margin-bottom: 12px;
  font-size: 18px;
  color:red;
}
.w3-container
{
  background-color: white;
 
  font-color:red;
  margin-top:15px;
}
span.w3-button.w3-red.w3-large.w3-display-topright {
    float: right;
    padding-right: 5px;
}
.w3-display-container
{
  height: 50px;
}
.w3-panel.w3-green.w3-display-container {
    background: palevioletred;
    text-align: center;
}
.w3-panel.w3-green.w3-display-container p{
    line-height: 30px;
}
</style>
</html>