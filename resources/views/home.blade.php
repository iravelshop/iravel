<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Iravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">

    <!-- Styles -->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}"/>
</head>
<body>
    <div id="app">
  
   <form id ="frmhome"  method="GET">               
<nav class="navbar navbar-inverse bg-dark">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle float-right">
         @if(isset($_GET['search']))
         <input type=text name=txtsearch id=txtsearch value={{$_GET['search']}}><a onclick="submitResponse()" id="search">
          @else
            <input type=text name=txtsearch id=txtsearch value=""><a onclick="submitResponse()" id="search">
          @endif
          <img src="/svg/search-icon.png"></a></button>
      <a class="navbar-brand" href="#">Iravel</a>
    </div>
  </div>
</nav>
<section class="home-wrapper">
  <div class="home-category bg-dark">
    <div class="container-fluid">
      <h2 class="white">Discover</h2>
      <div class="category-wrap">
       
        <ul>
           @foreach($category as $cat)
      <li><a href="/home?category={{$cat->_id}}" ><img src="{{ url('storage/'.$cat->catimage) }}"></li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="filter-wrap">
      <div class="pt-10 pb-10">
        <label>Popular in</label>
        <select class="locality">
          <option>Chennai</option>
          <option>Telangana</option>
          <option>Andra Predesh</option>
        </select>
      </div>
      <div id="SlideMiddle">
        <div id="nav">
          <div id="nav-bar">
              <div id="nav-bar-filters">
                @if(!isset($_GET['subcategory']))
                  
                <div data-f="*" class="filter-item active"><span><a href="/home" >All</a></span></div>
                @else
                <div data-f="*" class="filter-item "><span><a href="/home" >All</a></span></div>
              @endif
                @if(!empty($subcategory))
                 @foreach($subcategory as $subcat)
        
                  @if(isset($_GET['subcategory']) and (request()->get('subcategory') ==$subcat->_id ))
                <div data-f=".c0" class="filter-item active"><span><a href="/home?subcategory={{$subcat->_id}}" >{{$subcat->name}}

                  @else
                  <div data-f=".c0" class="filter-item"><span><a href="/home?subcategory={{$subcat->_id}}" >{{$subcat->name}}

                   @endif
                </a></span></div>
                @endforeach
                @endif
              </div>
          </div>
        </div>
        <div id="grid">
          <div id="grid-content">
            <?php $intcol=3 ;?>
           
              @if(!empty($products))
             @foreach($products as $prod)
            
              @if ($intcol%3==0)
                      <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))
                                    @foreach($products_image as $image)
                                         @if ($image->products_id == $prod->_id)
                                         @if ($prod->user_id!=Auth::user()->id)
                                        <div class="product-img">ll{{Auth::user()->id}}<a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">$ <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @elseif  ($intcol%3==1)
                  <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))
                                    @foreach($products_image as $image)
                                         @if ($image->products_id == $prod->_id)
                                        <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">$ <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @elseif  ($intcol%3==2)
                    <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))
                                    @foreach($products_image as $image)
                                         @if ($image->products_id == $prod->_id)
                                         <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">$ <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @endif
             @endforeach
             @endif
          </div>
        </div>
     </div>
    </div>
  </form>
</section>
<section class="home-footer">
  <div class="container-fluid">
    <ul>
      <li class="active"><a href="#"><img src="images/home-icon.png" /><span>Home</span></a></li>
      <li><a href="#"><img src="images/profile-icon.png" /><span>Profile</span></a></li>
      <li><a href="#"><img src="images/message-icon.png" /><span>Chat</span></a></li>
      <li class="cameraico"><a href="#"><img src="images/camara-icon.png" /><span>Capture</span></a></li>
    </ul>
  </div>
</section>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.0/isotope.pkgd.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js'></script>
<script  src="js/index.js"></script>
     <script>
      
      function submitResponse() {
        
    window.location.href = 'http://iravel.test:8000/home?search='+document.getElementById("txtsearch").value;
    document.frmhome.submit(); 
}
     </script>
    </div>
</body>
</html>

