<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Iravel</title>

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">

    <!-- Styles -->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 @laravelPWA

</head>
<style type="text/css">
  .dropdown
    {
        float: right;
    
  }
  .dropdown-item {
    color:#007f3d; }
  .btn-secondary {
     color:#007f3d; 
     background-color: #ffffff; 
    border-color: #ffffff; 
    width:190px;
}
.btn-secondary:not(:disabled):not(.disabled).active, .btn-secondary:not(:disabled):not(.disabled):active, .show>.btn-secondary.dropdown-toggle {
    color:#007f3d; 
     background-color: #ffffff; 
    border-color: #ffffff; 
}
button#dropdownMenu2 {
    padding-top: 2px;
    padding-right: 0px;
}
.popular
{
   font-size: 16px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.19;
  letter-spacing: 0.23px;
  text-align: left;
  color: #37434d;
}

/*@media screen and (max-width: 768px)
{
#SlideMiddle #grid-content .grid-item {
   
    margin: 0 auto;
    width: 45%;
}
}*/
</style>
<body>
    <div id="app">
  
   <form id ="frmhome"  method="GET">               
<nav class="navbar navbar-inverse bg-dark">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle float-right">
        <img src="/svg/shopping-cart.svg" />&nbsp;&nbsp;
         <a href="/search"> <img src="/svg/search-icon.png"></a></button>
      <a class="navbar-brand" href="#">Iravel</a>
      <li class="dropdown dropdown-notifications" style="display: none">
              <a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
                <i data-count="0" class="glyphicon glyphicon-bell notification-icon"></i>
              </a>

              <div class="dropdown-container">
                <div class="dropdown-toolbar">
                  <div class="dropdown-toolbar-actions">
                    <a href="#">Mark all as read</a>
                  </div>
                  <h3 class="dropdown-toolbar-title">Notifications (<span class="notif-count">0</span>)</h3>
                </div>
                <ul class="dropdown-menu">
                </ul>
                <div class="dropdown-footer text-center">
                  <a href="#">View All</a>
                </div>
              </div>
            </li>
    </div>
  </div>
</nav>
<section class="home-wrapper">
  <div class="home-category bg-dark">
    <div class="container-fluid">
      <h2 class="white">Discover</h2>
      <div class="category-wrap">
       
        <ul>
            @if (isset($_GET['city']))
           @foreach($category as $cat)
            
      <li><a href="/home?category={{$cat->_id}}&city={{$_GET['city']}}" ><img src="{{ url('storage/'.$cat->catimage) }}" /></a></li>
          @endforeach
          @else
              @foreach($category as $cat)
             <li><a href="/home?category={{$cat->_id}}" ><img src="{{ url('storage/'.$cat->catimage) }}" /></a></li>
          @endforeach
          @endif
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="filter-wrap">
      <div class="pt-10 pb-10">
        <label class="popular">Popular in</label>
<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Select Location 
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu2" id="dropdown-menu1">
       <a class="dropdown-item">Current location &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons">my_location</i>&nbsp;&nbsp;</a>
          @foreach($city as $cityrow)
          @if (isset($_GET['city']) && $_GET['city']==$cityrow)
           <a class="dropdown-item active" >{{$cityrow}}</a>
           @else
            <a class="dropdown-item " >{{$cityrow}}</a>
           @endif
        @endforeach
          
        </div>
      </div>
      <div id="SlideMiddle">
        <div id="nav">
          <div id="nav-bar">
              <div id="nav-bar-filters">
                @if(!isset($_GET['subcategory']))
                  
                <div data-f="*" class="filter-item active"><span><a href="/home" >All</a></span></div>
                @else
                <div data-f="*" class="filter-item "><span><a href="/home" >All</a></span></div>
              @endif
                @if (isset($_GET['city']))
                      @if(!empty($subcategory))
                       @foreach($subcategory as $subcat)
              
                        @if(isset($_GET['subcategory']) and (request()->get('subcategory') ==$subcat->_id ))
                      <div data-f=".c0" class="filter-item active"><span><a href="/home?subcategory={{$subcat->_id}}&city={{$_GET['city']}}" >{{$subcat->name}}

                        @else
                        <div data-f=".c0" class="filter-item"><span><a href="/home?subcategory={{$subcat->_id}}&city={{$_GET['city']}}" >{{$subcat->name}}

                         @endif
                      </a></span></div>
                      @endforeach
                      @endif
                @else

                      @if(!empty($subcategory))
                       @foreach($subcategory as $subcat)
              
                        @if(isset($_GET['subcategory']) and (request()->get('subcategory') ==$subcat->_id ))
                      <div data-f=".c0" class="filter-item active"><span><a href="/home?subcategory={{$subcat->_id}}" >{{$subcat->name}}

                        @else
                        <div data-f=".c0" class="filter-item"><span><a href="/home?subcategory={{$subcat->_id}}" >{{$subcat->name}}

                         @endif
                      </a></span></div>
                      @endforeach
                     
                @endif
                @endif
              </div>
          </div>
        </div>
        <div id="grid">
          <input type="hidden" name="hdnuserid" value="{{Auth::user()->id}}" id=hdnuserid>
          <div id="grid-content">
            <?php $intcol=3 ;?>
              @if(!empty($products))
             @foreach($products as $prod)
              @if ($intcol%3==0)
                      <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))
                                    @foreach($products_image as $image)
                                         @if ($image->products_id == $prod->_id)
                                       @if ($prod->user_id!=Auth::user()->id)
                                        <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                          <?php
                        // Example 1
                        $desc  = $prod->description;
                        $arrdesc = explode(" ", $desc);
                        $str='';
                        $strprev='';
                         for ($x = 0; $x < count($arrdesc); $x++) {
                          if (strlen($str.' '.$arrdesc[$x])<21)
                            $str=$str.' '.$arrdesc[$x];
                            else
                              break;

                         } 
                        
                        if (strlen($desc)<21)
                        echo  '<label class="product-title">'.$str.'</label>'; // piece1
                      else
                         echo  '<label class="product-title">'.$str.'.. </label>'; // piece1
                        

                        ?>
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">  @if(!empty($prod->currency)) 
                            {{$prod->currency}}
                            @else
                           {{'$'}}
                            @endif  <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @elseif  ($intcol%3==1)
                  <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))
                                    @foreach($products_image as $image)
                                         @if ($image->products_id == $prod->_id)
                                         @if ($prod->user_id!=Auth::user()->id)
                                        <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                         <?php
                        // Example 1
                        $desc  = $prod->description;
                        $arrdesc = explode(" ", $desc);
                        $str='';
                        $strprev='';
                         for ($x = 0; $x < count($arrdesc); $x++) {
                          if (strlen($str.' '.$arrdesc[$x])<21)
                            $str=$str.' '.$arrdesc[$x];
                            else
                              break;

                         } 
                        if (strlen($desc)<21)
                        echo  '<label class="product-title">'.$str.'</label>'; // piece1
                      else
                         echo  '<label class="product-title">'.$str.'.. </label>'; // piece1
                        ?>
                          
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">
                            @if(!empty($prod->currency)) 
                            {{$prod->currency}}
                            @else
                           {{'$'}}
                            @endif 
                            <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @elseif  ($intcol%3==2)
                    <div class="grid-item c{{$intcol%3}} wow fadeInUp" >
                         <?php $intcol++; ?>
                            @if(!empty($products_image))

                                    @foreach($products_image as $image)
                                   
                                         @if ($image->products_id == $prod->_id)

                                         @if ($prod->user_id!=Auth::user()->id)
                                        <div class="product-img"><a href="/details?id={{$prod->_id}}"><img src="{{ url('storage/'.$image->filename) }}" /></a></div>
                                        @else
                                         <div class="product-img"><img src="{{ url('storage/'.$image->filename) }}" /></div>
                                        @endif
                                        
                                        @endif
                                   @endforeach
                              @endif
                        <div class="product-desc">
                          <?php
                        // Example 1
                        $desc  = $prod->description;
                        $arrdesc = explode(" ", $desc);
                        $str='';
                        $strprev='';
                         for ($x = 0; $x < count($arrdesc); $x++) {
                          if (strlen($str.' '.$arrdesc[$x])<21)
                            $str=$str.' '.$arrdesc[$x];
                            else
                              break;

                         } 
                        
                        if (strlen($desc)<21)
                        echo  '<label class="product-title">'.$str.'</label>'; // piece1
                      else
                         echo  '<label class="product-title">'.$str.'.. </label>'; // piece1
                        ?>
                          <label class="product-title">{{$prod->title}}</label>
                          <label class="product-cost">  @if(!empty($prod->currency)) 
                            {{$prod->currency}}
                            @else
                           {{'$'}}
                            @endif <span>{{$prod->price}}</span></label>
                        </div>
                         
                      </div>
                  @endif
             @endforeach
             @endif
          </div>
        </div>
     </div>
    </div>
    <input type=hidden name=lat id=lat>
    <input type=hidden name=long id=long>
    <input type=hidden name=hdncity id=hdncity>
    <input type=hidden name=hdnstate id=hdnstate>
     <input type=hidden name=hdnCountry id=hdnCountry>
    <input type=hidden name=hdntown id=hdntown>
     <input type=hidden name=hdnloc id=hdnloc>
  </form>
</section>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.3.0/mapbox-gl-geocoder.min.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.3.0/mapbox-gl-geocoder.css' type='text/css' />
<script type="text/javascript">
 
  /*function citychange()
  {
   str=self.location.toString(); 
    if (str.indexOf('city') > -1)

     if (str.indexOf('?') > -1)
 self.location= self.location+"&city="+document.getElementById("frmcity").value;
     else
     self.location= self.location+"?city="+document.getElementById("frmcity").value;
     
     //frmhome.submit();
  }*/
</script>
 <!--<script src="https://js.pusher.com/4.2/pusher.min.js"></script>-->
    <link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap-notifications.min.css')}}">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript">
    
  /*    var notificationsWrapper   = $('.dropdown-notifications');
      var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
      var notificationsCountElem = notificationsToggle.find('i[data-count]');
      var notificationsCount     = parseInt(notificationsCountElem.data('count'));
      var notifications          = notificationsWrapper.find('ul.dropdown-menu');

      if (notificationsCount <= 0) {
        notificationsWrapper.hide();
      }

      // Enable pusher logging - don't include this in production
      // Pusher.logToConsole = true;

      var pusher = new Pusher('ddbf8feb78c9400225d0', {
      cluster: 'us2',
      forceTLS: true
    });

      // Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('booking.'+document.getElementById('hdnuserid').value);


      // Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\PostPublish',function(data){
        var existingNotifications = notifications.html();
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
        var newNotificationHtml = `
          <li class="notification active">
              <div class="media">
                <div class="media-left">
                  
                </div>
                <div class="media-body">
                  <strong class="notification-title">`+data.renter.name +" booked your product from "+data.booking.start_dt+" to "+data.booking.end_dt+`</strong>
                  <!--p class="notification-desc">Extra description can go here</p-->
                  <div class="notification-meta">
                    <small class="timestamp">about a minute ago</small>
                  </div>
                </div>
              </div>
          </li>
        `;
        notifications.html(newNotificationHtml + existingNotifications);

        notificationsCount += 1;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
        notificationsWrapper.show();
      });
   //  Pusher.logToConsole = true;
*/

$(document).ready(function(){
 $("#dropdownMenu2").text(getUrlParameter('city'));
     $("#dropdownMenu2").val(getUrlParameter('city'));
  $('#dropdown-menu1 a').click(function () {
         if (this.innerText.indexOf("Current location")>-1)
         {
             if(navigator.geolocation) {
                 navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
                 //strCity=document.getElementById("hdncity").value;
             } else {
                  alert("Please allow the application to track location by changing the settings in browser.");
                }
    
           strCity=document.getElementById("hdncity").value;

         }
         else
         {
          strCity=this.innerText;
          previous=getUrlParameter('city');
           str=self.location.toString(); 
              if (str.indexOf('city') > -1)
                self.location=str.replace(previous,strCity);
          else
          {
               if (str.indexOf('?') > -1)
           self.location= str+"&city="+strCity;
               else
               self.location= str+"?city="+strCity;
               
            }
            previous =strCity;
        }
        console.log(strCity)
         /* previous=getUrlParameter('city');
           str=self.location.toString(); 
              if (str.indexOf('city') > -1)
                self.location=str.replace(previous,strCity);
          else
          {
               if (str.indexOf('?') > -1)
           self.location= str+"&city="+strCity;
               else
               self.location= str+"?city="+strCity;
               
            }
            previous =strCity;
         */
    });
});
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
        
function geoSuccess(position) {
 // console.log(position);
    document.getElementById('lat').value= position.coords.latitude;
    document.getElementById('long').value = position.coords.longitude; 
    if ( position.coords.latitude!="")
    { $.getJSON('https://api.tiles.mapbox.com/v4/geocode/mapbox.places/'+position.coords.longitude+','+position.coords.latitude+'.json?access_token=pk.eyJ1IjoicmFteWFrIiwiYSI6ImNqdzF1cnAyODAyb28zenFyZTFvYjZ5aDEifQ.6_FhtdOUY4keozu6y5EeaQ', function(data) {
  
     parseReverseGeo(data.features[0]);
     strCity=document.getElementById("hdncity").value;
previous=getUrlParameter('city');
           str=self.location.toString(); 
              if (str.indexOf('city') > -1)
                self.location=str.replace(previous,strCity);
          else
          {
               if (str.indexOf('?') > -1)
           self.location= str+"&city="+strCity+"&lat="+document.getElementById("lat").value+"&long="+document.getElementById("long").value+"&town="+document.getElementById("hdntown").value+"&state="+document.getElementById("hdnstate").value+"&ctry="+document.getElementById("hdnCountry").value;
               else
               self.location= str+"?city="+strCity+"&lat="+document.getElementById("lat").value+"&long="+document.getElementById("long").value+"&town="+document.getElementById("hdntown").value+"&state="+document.getElementById("hdnstate").value+"&ctry="+document.getElementById("hdnCountry").value;
               
            }
            previous =strCity;

     });
  //return strcity;
  }
}

function geoError(error) {
   
   switch (error.code)
  {
    case error.PERMISSION_DENIED:
      // User denied access to location. Perhaps redirect to alternate content?
      alert('Please allow the application to track location by changing the settings in browser.');
      break;
    case error.POSITION_UNAVAILABLE:
      alert('Position is currently unavailable.');
      break;
    case error.PERMISSION_DENIED_TIMEOUT:
      alert('User took to long to grant/deny permission.');
      break;
    case error.UNKNOWN_ERROR:
      alert('An unknown error occurred.')
      break;
    }
}
function parseReverseGeo(geoData) {
                    // debugger;
                    var state,city,town, countryName, placeName, returnStr;
                   town=geoData.text;
                    if(geoData.context){

                        $.each(geoData.context, function(i, v){
                            if(v.id.indexOf('region') >= 0) {
                                state = v.text;
                            }
                             if(v.id.indexOf('district') >= 0 || v.id.indexOf('place') >= 0) {
                                city = v.text;
                            }
                            if(v.id.indexOf('country') >= 0) {
                                countryName = v.text;
                            }
                        });
                    }

                    if(town && state && countryName) {

                      document.getElementById('hdntown').value=town;
                      document.getElementById('hdncity').value=city;
                      document.getElementById('hdnstate').value=state;
                      document.getElementById('hdnCountry').value=countryName;
                     // return city;
                       // returnStr = town + ','+ city + ","+state + "," + countryName;
                    } else {
                      document.getElementById('hdnloc').value=geoData.place_name;
                     // return geoData.place_name;
                        
                    }
                   // return '';
                   
                }
    </script>
    <style type="text/css">
    i.material-icons {
   
    position: absolute;
}
a:not([href]):not([tabindex]) {
    color: #007f3d;
    text-decoration: none;
}
    
.btn-secondary:hover{
     color:#007f3d; 
     background-color: #ffffff; 
    border-color: #ffffff; 
}
      li.dropdown.dropdown-notifications {
    /* width: 18%; */
    float: right;
    display: inline-block;
    position: relative;
  }
  .dropdown-notifications>.dropdown-container, .dropdown-notifications>.dropdown-menu {
    width: 300px;
    max-width: 300px;
}

    </style>
@extends('layouts.footer')
