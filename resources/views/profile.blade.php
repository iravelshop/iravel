
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Account and Profile page</title>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv=“Pragma” content=”no-cache”>
<meta http-equiv=“Expires” content=”-1″>
<meta http-equiv=“CACHE-CONTROL” content=”NO-CACHE”>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/custom.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/responsive.min.css"/>
 
   @laravelPWA
</head>
<body class="product-fullview">
  <div class="container-fluid bg-dark">
    <div class="navbar-header ">
      <div class="col-xs-4">
        <a class="back-btn pt-10 pb-10" href="/home" style="display: table; line-height: 36px;"><img src="/svg/1-a.svg"></a>
      </div>
      <div class="col-xs-4"><h2 class="product-head">Account</h2></div>
      <div class="col-xs-4 pt-10 pb-10 product-search "><a href="/profileedit"><img src="/svg/profile-edit.svg" class="pull-right"></a></div>
    </div>
  </div>
<div class="container-fluid">
  <div class="profile-acc-view">
  	<div class="col-md-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-12 profile-acc-pic">
       @if(!empty($users))
         @foreach($users as $user)
         @if(!empty($user->avatar))
  		<img src={{ url('storage/'.$user->avatar) }}>
      @endif
  	</div>
  	<div class="col-sm-5 col-xs-12 profile-acc-desc">
        
  		<h3>{{$user->name}}</h3>
         @if(!empty($user->interest))
  		        <p>My name is {{$user->name}}, I'm interested in {{$user->interest}}</p>
               @endif
       @endforeach
      @endif
       @if(!empty($products))
  		<div class="acc-info"><span>{{$products->count()}}</span> Collection</div>
      @else
        <div class="acc-info"><span>0</span> Collection</div>
      @endif
       @if(isset($follow) && ($follow->count()>0))
  		<div class="acc-info"><span>{{$follow->count()}}</span> Friends</div>
     
       @endif
  	</div>
  </div>
</div>
  <hr>
  <div id="emptydiv"  class="wd-80-auto" style="text-align:center;display:none">
    <img src="/svg/Empty.svg"
     class="Empty" />
     <p>Please add products to create collections</p>
     <br/>
     <a href="{{url('products/create')}}" class="btn-lg-custom e createcol">Create Collection</a>
     <br/>

  </div>
<div class="container-fluid"  id="collections">
  <div class="collection-wrapper">
  	<div class="collection-header"><h5><b>Collections</b></h5>
        @if(!empty($products))
        <h5 class="pull-right">{{$products->count()}}</h5>
      @else
    <h5 class="pull-right">0</h5>
    @endif
  </div>
  	<div class="clear-fix"></div>
     
       @if(!empty($products))
          @foreach($products as $prod)
          
           <div class="collection-item list">
            <h4><a href='{{ url("products/create?id=$prod->draft_id") }}'>{{$prod->title}}</a> &nbsp;&nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;&nbsp;&nbsp; 
              <a href='/bookings?prodid={{$prod->_id}}' style="color:#007f3d">Booking Details</a></h4>
             @if(!empty($products_image))
          @foreach($products_image as $img) 
              @if($img->products_id==$prod->_id)

               <img src="{{ url('storage/'.$img->filename) }}">
               @endif
       
           @endforeach
          @endif
          </div>
   
      @endforeach
     @endif
    
  
  </div>
</div>  
<div class="container-fluid"  id="draftcollections">
  <div class="collection-wrapper">
    <div class="collection-header"><h5><b>Collections In Draft</b></h5>
        @if(!empty($products_draft))
        <h5 class="pull-right">{{$products_draft->count()}}</h5>
      @else
    <h5 class="pull-right">0</h5>
    @endif
  </div>
    <div class="clear-fix"></div>
     
       @if(!empty($products_draft))
          @foreach($products_draft as $prod)
          
           <div class="collection-item list ">
            <h4><a href='{{ url("products/create?id=$prod->id") }}'>{{$prod->title}}</a></h4>
             @if(!empty($productsdf_image))
          @foreach($productsdf_image as $img) 
              @if($img->products_draft_id==$prod->_id )

               <img src="{{ url('storage/'.$img->filename) }}">
               @endif
       
           @endforeach
          @endif
          </div>
   
      @endforeach
     @endif
    
  
  </div>
</div>  
<div class="container-fluid" id="bookings">
  <div class="collection-wrapper">
    <div class="collection-header"><h5><b>My Bookings</b></h5>
        @if(!empty($products_draft))
        <h5 class="pull-right">{{$booking->count()}}</h5>
      @else
    <h5 class="pull-right">0</h5>
    @endif
  </div>
    <div class="clear-fix"></div>
     
       @if(!empty($booking))
          @foreach($booking as $book)
          
           <div class="collection-item">
          

              @if(!empty($products_image))
          @foreach($products_image as $img) 
              @if($img->products_id==$book->product_id && $img->position==1)

               <a href="/mybookings?bookingid={{$book->_id}}"><img src="{{ url('storage/'.$img->filename) }}"></a>
               @endif
       
           @endforeach
          @endif
          <p><h5 >
          Booking from {{$book->start_dt}} to {{$book->end_dt}}</h5></p>
          <br/>
          </div>
   
      @endforeach
     @endif
    
  
  </div>
</div>  

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 

</body>
<style type="text/css">
 #emptydiv
 {
  padding-bottom: 15px;
 }
 .Empty
 {
   width: 200px;
 
  height: 156.5px;
 
}
#emptydiv p
{

  opacity: 0.5;
  font-family: Lato;
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: 0.2px;
  text-align: center;
  color: #282c40;
}
#emptydiv button
{
  width: 303px;
  height: 50px;
}
 </style>
 <script type="text/javascript">
   $( document ).ready(function() {
        if ($(".collection-item.list")[0]){
          

        }
        else
        {
          emptydiv.style.display="block";
          collections.style.display="none";
          draftcollections.style.display="none"
        }
        if ($(".collection-item")[0]){
        }
        else
        {
              bookings.style.display="none";
        }

    });
 </script>
</html>