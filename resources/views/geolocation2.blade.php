<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Home Intro</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <script src="{{URL::asset('js/addtohomescreen.js')}}" ></script>
    <link href="{{ URL::asset('css/addtohomescreen.css') }}" rel="stylesheet">  
 <script type="text/javascript">
 addToHomescreen();
 </script>
   @laravelPWA
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
  <link rel=manifest href="{{ URL::asset('js/manifest.json') }}">
  <link href="{{ URL::asset('css/responsive.css') }}" rel="stylesheet">  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 

</head>
<body>
<div class="pt-30"></div>
<div class="container width-md-30 width-sm-100">
  <div id="carouselFade" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carouselFade" data-slide-to="0" class="active"></li>
      <li data-target="#carouselFade" data-slide-to="1"></li>
      <li data-target="#carouselFade" data-slide-to="2"></li>
      <li data-target="#carouselFade" data-slide-to="3"></li>
      <li data-target="#carouselFade" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <div class="into-img"><a href="#"><img src="/svg/screen-1.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Easily search what you want</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> </div>
        <div class="into-next">
          <a class="right carousel-control" href="#carouselFade" data-slide="next">
          <img src="/svg/next-arrow.png" />
        </a></div>
        </div>

      <div class="item">
        <div class="into-img"><a href="#"><img src="/svg/screen-2.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Interact with Trusted users</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
          <div class="into-next"><a class="right carousel-control" href="#carouselFade" data-slide="next">
       <img src="/svg/next-arrow.png" />
      </a></div>
      </div>
    
      <div class="item">
        <div class="into-img"><a href="#"><img src="/svg/screen-3.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Locate where the product is</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
         <div class="into-next"><a class="right carousel-control" href="#carouselFade" data-slide="next">
     <img src="/svg/next-arrow.png" />
      </a></div>
      </div>

      <div class="item">
        <div class="into-img"><a href="#"><img src="/svg/screen-4.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Save money with this app with rentals</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
          <div class="into-next"><a class="right carousel-control" href="#carouselFade" data-slide="next">
          <img src="/svg/next-arrow.png" />
      </a></div>
      </div>
    
      <div class="item">
        <div class="into-img"><a href="#"><img src="/svg/screen-5.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Feel free to share this app with friends</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
          <div class="into-next"><a class="right carousel-control" href="/welcome" data-slide="next">
       <img src="/svg/next-arrow.png" />
      </a></div>
      </div>

    <!-- Left and right controls -->
 <!--    <div class="into-next">
          <a class="right carousel-control" href="#carouselFade" data-slide="next">
          <img src="images/next-arrow.png" />
        </a></div> -->
  </div>
    </div>
<div class="skip-link"><a href="/welcome">Skip >></a></div>
</div>
<div id="popup">
    <div class="popup-close-icon">&times;</div>
    
    <p><img src="/svg/addtohomescreen.png" alt="" />Install this webapp on your iPhone: tab <img src="/svg/starticon.png" alt="" />and then Add to homescreen</p>
  </div>

<!-- <div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>

    <div class="carousel-inner">
      <div class="item active">
        <div class="into-img"><a href="#"><img src="images/screen-1.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Easily search what you want</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> </div>
        <div class="into-next">
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <img src="images/next-arrow.png" />
        </a></div>
        </div>

      <div class="item">
        <div class="into-img"><a href="#"><img src="images/screen-2.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Interact with Trusted users</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
          <div class="into-next"><a class="right carousel-control" href="#myCarousel" data-slide="next">
       <img src="images/next-arrow.png" />
      </a></div>
      </div>
    
      <div class="item">
        <div class="into-img"><a href="#"><img src="images/screen-3.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Locate where the product is</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
         <div class="into-next"><a class="right carousel-control" href="#myCarousel" data-slide="next">
     <img src="images/next-arrow.png" />
      </a></div>
      </div>

      <div class="item">
        <div class="into-img"><a href="#"><img src="images/screen-4.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Save money with this app with rentals</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
          <div class="into-next"><a class="right carousel-control" href="#myCarousel" data-slide="next">
          <img src="images/next-arrow.png" />
      </a></div>
      </div>
    
      <div class="item">
        <div class="into-img"><a href="#"><img src="images/screen-5.png" alt="" style="width:100%;"></a></div>
        <div class="into-desc"><h3>Feel free to share this app with friends</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
          <div class="into-next"><a class="right carousel-control" href="login.html" data-slide="next">
       <img src="images/next-arrow.png" />
      </a></div>
      </div>
    </div>

   
  </div>
</div> -->
</body>
 <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.3.0/mapbox-gl-geocoder.min.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.3.0/mapbox-gl-geocoder.css' type='text/css' />

  

  <script>
   try{Typekit.load();}catch(e){
}

$(function() {
 // if (("standalone" in window.navigator) && !window.navigator.standalone)
   {
  $('#popup').animate({'bottom': '-14%'}, 1000).animate({'bottom': '-4%'}, 75).animate({'bottom': '-14%'}, 75);
  $('.popup-close-icon').on('click', function(){
    $('#popup').animate({'bottom': '1000px'}, 500, function() {
      $(this).remove();
    });
  });
}
});

  </script>
<style type="text/css">

#container {
  width: 320px;
  height: 240px;
  background-color: #126697;
  text-align: center;
  h1 {
    font-family: "coquette";
    color: white;
    text-shadow: 0px -2px 0px #000;
  }
}

#popup {
  position: relative;
  bottom: 1000px;

  width: 98%;
  height: 80px;
  margin-left:20px;
  margin-right:20px;
  background-color: #ffffff;
  background-image: linear-gradient(top, #f97d4f 0%,#fc5d23 50%,#fd4703 51%,#fd4703 100%);
  border: 2px solid #00234C;
  -webkit-border-radius: 5px;
  z-index: 9999;
  text-align: center;
  -webkit-box-shadow :10px 10px 30px #333, -10px -10px 30px #333, -10px 10px 30px #333, 10px -10px 30px #333;
  

 
}
#popup::after {
    position: absolute;
    content: '';
    height: 0px;
    width: 0px;
    left: 43%;
    bottom: -20px;
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
    border-top: 20px solid #ffffff;
    z-index: 9999;
  }
#popup  h4 {
    color: black;
   
    font-size: 18px;
    font-family: Belleza;
    margin-top: 0;
    margin-bottom: 0;
  }
  #popup p {
    color: grey;
  
    font-size: 14px;
    
    font-family: Helvetica, Arial, sans-serif;
  }
  #popup img {
    height:50px;
    width:50px;
  }
  
.popup-close-icon {
  position: absolute;
  width: 0px;
  right: 20px;
  top: 0px;
  font-size: 24px;
  font-weight: bolder;
  color: black;
  text-shadow: 0px -1px 1px #00234C;
  cursor: pointer;
}

.bottom-bar {
  position: absolute;
  bottom: 8%;
  left: 8px;
}
</style>

</html>