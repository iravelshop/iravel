<!DOCTYPE html>
<html lang="en">
<head>
  <title>Iravel - Home Intro</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="{{ URL::asset('css/custom.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('css/responsive.min.css') }}" rel="stylesheet">
 @laravelPWA
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
        $('#carouselFade').carousel();
    });
 </script>
  <style type="text/css">
  a.btn-lg-custom.g {
    display: inline-block;
    text-align: center;
}
a.btn-lg-custom.e {
    display: inline-block;
    text-align: center;
}
 </style>
</head>
    <body>
     
        <div class="pt-30"></div>
<section class="signing-wrapper">
  <div class="container">
    <div class="wd-80-auto">
      <h3>Sharing App</h3>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      <img src="/svg/screen-5.png" class="img-responsive signup-img">

    </div>
     @if (Route::has('login'))
                
                    @auth
<p class="text-center login-link pt-30">                   <a href="{{ url('/home') }}">Home</a></p>
	    @else
                        
                      

                        @if (Route::has('register'))
                         <div class"introsign"><a  class="btn-lg-custom e" onclick="location.href='{{ url('register') }}'">Sign Up</a><br/></div>
 <div class"introsign"><a href="/redirect" class="btn-lg-custom g">Login With Google</a></div>
    <p class="text-center login-link pt-30">
      Already have an account? <a href="{{ url('/home') }}">Login</a>
    </p>
  
                            @endif
                    @endauth
                
            @endif
   <!-- <input type=hidden name=lat id=lat>
    <input type=hidden name=lat id=long>
    <input type=hidden name=location id=location>-->
     
  </div>
</section>
<div id="popup" style="display: none;bottom:4%">
    <div class="popup-close-icon">&times;</div>
    
    <p><img src="/svg/addtohomescreen.png" alt="" />Install this webapp on your iPhone: tab <img src="/svg/starticon.png" alt="" style="width:30px;margin-right:5px" /> and then Add to Home Screen</p>
  </div>
</div>
</form>
        
    </body>
   
<script>
     
   try{Typekit.load();}catch(e){
}

$(function() {

  // Detects if device is on iOS 
const isIos = () => {
  const userAgent = window.navigator.userAgent.toLowerCase();
  return /iphone|ipad|ipod/.test( userAgent );
}
// Detects if device is in standalone mode
const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);

// Checks if should display install popup notification:
if (isIos() && !isInStandaloneMode()) {
     $('#popup').show();
    
  $('#popup').animate({'bottom': '4%'}, 1000).animate({'bottom': '4%'}, 75).animate({'bottom': '4%'}, 75);
  $('.popup-close-icon').on('click', function(){
    $('#popup').animate({'bottom': '1000px'}, 500, function() {
      $(this).remove();
    });
  });
}
});


      function submitResponse() {
        
    window.location.href = 'https://iravelshop.com/redirect?lat='+document.getElementById("lat").value+"&long="+document.getElementById("long").value+"&location="+ document.getElementById('location').value
    //document.frmsubmit.submit(); 
}
     </script>
     <style type="text/css">
       #popup {
  position: absolute;
 

 
  height: 105px;
 
  background-color: #ffffff;
  background-image: linear-gradient(top, #f97d4f 0%,#fc5d23 50%,#fd4703 51%,#fd4703 100%);
  border: 2px solid #00234C;
  -webkit-border-radius: 5px;
  z-index: 9999;
  text-align: center;
  -webkit-box-shadow :10px 10px 30px #333, -10px -10px 30px #333, -10px 10px 30px #333, 10px -10px 30px #333;
  

 
}
#popup::after {
    position: absolute;
    content: '';
    height: 0px;
    width: 0px;
    left: 43%;
    bottom: -20px;
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
    border-top: 20px solid #ffffff;
    z-index: 9999;
  }
#popup  h4 {
    color: black;
   
    font-size: 18px;
    font-family: Belleza;
    margin-top: 0;
    margin-bottom: 0;
  }
  #popup p {
    color: grey;
      margin-top: 15px;
    font-size: 14px;
    line-height: 14px;
    font-family: Helvetica, Arial, sans-serif;
  }
  #popup img {
    height:50px;
    width:50px;
  }
  
.popup-close-icon {
  position: absolute;
  width: 0px;
  right: 20px;
  top: 0px;
  font-size: 24px;
  font-weight: bolder;
  color: black;
  text-shadow: 0px -1px 1px #00234C;
  cursor: pointer;
}

     </style>
</html>
