<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model;


class VerifyUser extends Model
{
    //

    protected $table = 'VerifyUser';
     protected $fillable = [
      'user_id',
      'token',
     
    
    ];
public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
