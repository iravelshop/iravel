<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payee;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use App\products;
use App\Invoice;
use App\products_image;
use App\subcategory;
use App\User;
use App\Follow;
use App\Review;
use App\book_product;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Events\PostPublish;
use Log;

class DetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $useremail;
    private $renteremail='';
    private $prodtitle='';
    private $amt='';
    private $currency='';
    private $bookingid;
    private $prevamt;
public function __construct()
{
/** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
      
        $this->_api_context->setConfig($paypal_conf['settings']);
  }
   public function index()
    {
        $proddetails=  products::where('_id', '=', $_GET['id'])->get();
        $prodimage=  products_image::where('products_id', '=', $_GET['id'])->get(); 
        $booking=  book_product::where('product_id', '=', $_GET['id'])->Where('user_id','<>',Auth::id())->get(); 
        $ldate = date('Y-m-d');
        
        $booking1=  book_product::where('product_id', '=', $_GET['id'])->Where('user_id',Auth::id())->Where('end_dt','>=',$ldate)->get(); 
        //$prod=compact('proddetails');
        $strdates='';
        foreach($booking as $book)
        {
          //  print_r($book);
              $sdt=$book->start_dt;
               $edt=$book->end_dt;
               $strdates= $strdates.','.$this->date_range($sdt,$edt);
        }
        //$arydates=explode(",",ltrim($strdates,","));
        $arydates=explode(",",ltrim($strdates,","));
        foreach($proddetails as $det)
        {
           // print_r($det);
              $userid=$det->user_id;
        }
        
         $user=  user::where('_id', '=',  $userid)->get();           
        $follow=follow::where('user_id','=',Auth::id())->where('follow_id','=',$userid)->get();
        $review=review::where('productid','=',$_GET['id'])->get();
        $intcount=0;
        $sum=0;
         foreach($review as $rev)
        {
          //  print_r($book);
              $sum=$sum+$rev->rating;
                $intcount++;

        }
        if ($sum >0 && $intcount>0)
       $ratingval=$sum/$intcount;
   else
    $ratingval=0;
      /* if ($_GET['id']=='5d1e111f4ae6de0df3082782')
         return view('details.index1',compact('proddetails','prodimage','user','arydates','booking1','follow','ratingval'));
     else*/
        return view('details.index',compact('proddetails','prodimage','user','arydates','booking1','follow','ratingval'));

    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
        

    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
   
        if ($request->bookingid==0)
       {
            
             $booking= book_product::create([
                             'title'=> $request->hdntitle,
                            'start_dt'=>$request->tstart,
                            'end_dt'=>$request->tend,
                          'product_id'=>$request->hdnid,
                           'user_id'=>Auth::id(),
                            'noofdays'=>$request->hdnNoofdays,
                            'total'=>$request->hdnTotalPrice,
                         ]
                        );
             $this->prevamt=0;
                       $this->bookingid=$booking->_id;
            // event(new PostPublish($user,$booking,$renter));
            }
            else
            {

                         $booking= book_product::where('_id', '=', $request->bookingid)->first();
                         $booking->start_dt=$request->tstart;
                            $booking->end_dt=$request->tend;
                             $booking->noofdays=$request->hdnNoofdays;
                            $booking->total=$request->hdnTotalPrice;
                $booking->save();
                 $this->bookingid=$request->bookingid;
               
            }

//$this->payWithpaypal();
            //return redirect('home');
          // return redirect()->action('PaymentController@payWithpaypal',['user'=>'ramya','renter'=>'raghav']);
    }
                   
    public  function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

   // $dates = array();
                $current = strtotime($first);
                $last = strtotime($last);
            $dates="";
                while( $current <= $last ) {

                    $dates = $dates.",".date($output_format, $current);
                    $current = strtotime($step, $current);
                }

                return ltrim($dates,",");
    } 

    public function userbookings() 
    {
        $prodid= $_GET['prodid'];
       $proddetails=  products::where('_id', '=', $prodid)->get();
        $prodimage=  products_image::where('products_id', '=', $prodid)->where('position','=',1)->get(); 
        $booking=  book_product::where('product_id', '=', $prodid)->Where('user_id','<>',Auth::id())->get(); 
        
        
    
        $rentee=  User::Where('_id','<>',Auth::id())->get();
       
        return view('details.bookingdetails',compact('proddetails','prodimage','booking','rentee'));
    }  
     public function mybookings() 
    {
        $prodid= $_GET['bookingid'];
         Log::info('booking done and redirect to mybboking'. $_GET['bookingid']);
         $bookings=  book_product::where('_id', '=', $prodid)->first(); 
         $booking=  book_product::where('_id', '=', $prodid)->get();
       $proddetails=  products::where('_id', '=', $bookings->product_id)->get();
        $prodimage=  products_image::where('products_id', '=',$bookings->product_id)->where('position','=',1)->get(); 
        $review=  review::where('bookingid', '=',$prodid)->get(); 
       
        
        
    
        $rentee=  User::Where('_id','<>',Auth::id())->get();
       
        return view('details.mybookingdetails',compact('proddetails','prodimage','booking','rentee','review'));
    } 
    public function rating() 
    {
        $prodid= $_GET['bookingid'];
         Log::info('booking done and redirect to mybboking'. $_GET['bookingid']);
         $bookings=  book_product::where('_id', '=', $prodid)->first(); 
         $booking=  book_product::where('_id', '=', $prodid)->get();
       $proddetails=  products::where('_id', '=', $bookings->product_id)->get();
        $prodimage=  products_image::where('products_id', '=',$bookings->product_id)->get(); 
        $review=  review::where('bookingid', '=',$prodid)->get(); 
       
         foreach($proddetails as $det)
        {
           // print_r($det);
              $userid=$det->user_id;
        }
        
         $rentee=  user::where('_id', '=',  $userid)->get();  
        
    
       
        return view('details.rating',compact('proddetails','prodimage','booking','review','rentee'));
    }                     

    //

  public function payWithpaypal(Request $request)
    {
         $user=User::where('_id','=',$request->ownerid)->first();
       $renter=User::where('_id','=',Auth::id())->first();
        
     $this->useremail=$user['email'];
    $this->renteremail=$renter['email'];
         $this->prodtitle=$request->hdntitle;

                if ($user['currency']=='$')
                     $this->currency='USD';
                else
                     $this->currency='INR';
        if ($request->bookingid!=0)
       {
        $books=book_product::where('_id','=',$request->bookingid)->first();
        $this->prevamt=$books->total;
       }
                
         if ($request->hdnTotalPrice>$this->prevamt)
                $this->amt=$request->hdnTotalPrice-$this->prevamt;
            else
               $this->amt=$this->prevamt-$request->hdnTotalPrice;
               
        /*print($this->useremail);
        print($this->currency);
        print($this->prodtitle);
        print($this->amt);*/
      //  exit;
       $this->store($request);
 Log::info('booking store');
       
        $payee = new Payee();
        $payee->setEmail('ramya13@gmail.com');

       // $payee->setEmail('k_r_ramya@yahoo.com');
        $payer = new Payer();
                $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName($this->prodtitle) /** item name **/
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($this->amt); /** unit price **/
        $item_list = new ItemList();
                $item_list->setItems(array($item_1));
               
        $amount = new Amount();
                $amount->setCurrency('USD')
                    ->setTotal($this->amt);

        $transaction = new Transaction();
                $transaction->setAmount($amount)
                ->setPayee($payee)
                    ->setItemList($item_list)
                    ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
                $redirect_urls->setReturnUrl(URL::route('getPaymentStatus')) /** Specify return URL **/
                    ->setCancelUrl(URL::route('getPaymentStatus'));
        $payment = new Payment();
                $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));

                /** dd($payment->create($this->_api_context));exit; **/
                try {
        $payment->create($this->_api_context);
        Log::info('payment create');

        } 
        catch (\PayPal\Exception\PPConnectionException $ex) {
          
                if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                                return Redirect::route('paypal');
                } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                                return Redirect::route('paypal');
                }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
            $redirect_url = $link->getHref();
                            break;
            }
        }
       
        /** add payment ID to session **/
                Session::put('booking_id', $this->bookingid);
                Log::info('payment for booking id'.$this->bookingid);
                 $invoice= invoice::create([
                             'paymentid'=> $payment->getId(),
                            'bookingid'=> $this->bookingid,
                            'status'=>'success'
                         ]
                        );
                 Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
        /** redirect to paypal **/
                    return Redirect::away($redirect_url);
        }
       
        \Session::put('error', 'Unknown error occurred');
                return Redirect::route('paywithpaypal');
        }
     public function getPaymentStatus()
    {

  
                /** Get the payment ID before session clear **/
               
              if(Session::has('paypal_payment_id')) 
              {
              // $payment_id = Session::get('paymentId');
                 $payment_id = Session::get('paypal_payment_id'); 
              }
              else
                $payment_id = $_GET['paymentId'];

             $inv=Invoice::where('paymentid','=',$payment_id)->first();

              if(Session::has('booking_id')) 
              {
              // $payment_id = Session::get('paymentId');
                 $bookid = Session::get('booking_id'); 
              }
              else
                $bookid = $inv['paymentId'];

        /** clear the session payment ID **/
                
                if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
                        \Session::put('error', 'Payment failed');
                         return view('details.afterpayments');
                  //  return Redirect::route('/paymentthanks');
                }

                //$payment = Payment::get('PAYID-LU5QHYQ7MX16279CD2720046');
                $payment = Payment::get($payment_id, $this->_api_context);
                $execution = new PaymentExecution();
                $execution->setPayerId(Input::get('PayerID'));
                /**Execute the payment **/
                $result = $payment->execute($execution, $this->_api_context);
                if ($result->getState() == 'approved') {
                     Log::info('after payment booking id'.$bookid);
                    Session::forget('paypal_payment_id');
                     //Session::forget('bookid');
                    \Session::put('success', 'Payment success');
                    return redirect('mybookings?bookingid='.$bookid);
                    // return view('details.afterpayments');
                            //return Redirect::route('/paymentthanks');
                }
                Session::forget('paypal_payment_id');
                 Log::info(' payment failed booking id'.$bookid);
                // Session::forget('bookid');
                 book_product::where('_id',$bookid)->delete();
                \Session::put('error', 'Payment failed');
                return view('details.afterpayments');
    }
    
}

