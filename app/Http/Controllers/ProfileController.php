<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Auth;

use App\products;
use App\products_image;
use App\subcategory;
use App\products_draft;
use App\products_draft_image;
use App\Follow;
use App\book_product;
use Session;
use Image;
class ProfileController extends Controller
{
    
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $users=User::where ('_id','=',Auth::id())->get();
       return view('profileedit',compact('users'));
    }
    public function profile()
    {
        $users=User::where ('_id','=',Auth::id())->get();
         $subcategory=subcategory::all();
         $products = products::where('user_id', '=', Auth::id())->where('status','=','published')->get();
         $products_image = products_image::get();
          $products_draft = products_draft::where('user_id', '=', Auth::id())->where('status','=','draft')->get();
          $productsdf_image = products_draft_image::get();
           $follow=follow::where('follow_id','=',Auth::id())->get();
            $booking=  book_product::Where('user_id','=',Auth::id())->get(); 
        return view('profile',compact('users','subcategory','products','products_image','products_draft','productsdf_image','follow','booking'));
       
    }
   
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename ="";
         if ($request->hdnSnap!='') {
         	 Session::put('image',$request->hdnSnap);

             if ($request->paypal!='')
                 {   
                    $olddata=User::where ('_id','=',Auth::id())->first();

		                 	if ($olddata->paypalid!=$request->paypal)
		                 	{
			                    $paypalexist=$this->userpaypal($request->paypal) ;
			                   
			                     if ($paypalexist=='Success')
			                     {
			                        User::where('_id','=',Auth::id())->update(['name'=>$request->uname,'age' => $request->age,'email' => $request->email,'phone'=>$request->phone,'avatar' => $filename,'interest'=>$request->interest,'paypalid'=>$request->paypal,'paypalverified'=>1]);   
			                     }
			                     else
			                     {
			                        $payuser= User::where('_id','=',\Auth::id())->update([
			                                            
			                                            'paypalverified'=>-1,
			                                            
			                                        ]); 
				                        Session::put('name',$request->uname);
                                        Session::put('avatar',$request->hdnSnap);
				                	  Session::put('age',$request->age);
				                	   Session::put('email',$request->email);
				                	   Session::put('phone',$request->phone);
				                	   Session::put('interest',$request->interest);
				 						Session::put('paypalid',$request->paypal);
				                         Session::put('paypalverify','Please enter valid PayPal Account Email Id.');
				                         return redirect('/profileedit');   
			                     }
		                 }
				    	else
		                {
		                 			// $filename = $request->upload->store('profile');
                      $image = Image::make($request->hdnSnap);
                                 $filename=Auth::id().'.jpg';
                                $image->save(storage_path().'/app/public/profile/'.$filename);
   
                                 $filename1='profile/'.Auth::id().'.jpg';
                            User::where('_id','=',Auth::id())->update(['name'=>$request->uname,'age' => $request->age,'email' => $request->email,'phone'=>$request->phone,'avatar' => $filename1,'interest'=>$request->interest]);   
		                }
		           }
                 else
                 {
                            $image = Image::make($request->hdnSnap);
                                 $filename=Auth::id().'.jpg';
                                $image->save(storage_path().'/app/public/profile/'.$filename);
                                $filename1='profile/'.Auth::id().'.jpg';
                            User::where('_id','=',Auth::id())->update(['name'=>$request->uname,'age' => $request->age,'email' => $request->email,'phone'=>$request->phone,'avatar' => $filename1,'interest'=>$request->interest]);   
                  }
             }
             else
             {
                if ($request->paypal!='')
                 {   
                 	$olddata=User::where ('_id','=',Auth::id())->first();

                 	if ($olddata->paypalid!=$request->paypal)
                 	{
	                    $paypalexist=$this->userpaypal($request->paypal) ;
	                   
	                     if ($paypalexist=='Success')
	                     {
	                        User::where('_id','=',Auth::id())->update(['name'=>$request->uname,'age' => $request->age,'email' => $request->email,'phone'=>$request->phone,'interest'=>$request->interest,'paypalid'=>$request->paypal,'paypalverified'=>1]);   
	                     }
	                     else
	                     {
	                        $payuser= User::where('_id','=',\Auth::id())->update([
	                                            
	                                            'paypalverified'=>-1,
	                                            
	                                        ]); 
		                        Session::put('name',$request->uname);
		                	  Session::put('age',$request->age);
		                	   Session::put('email',$request->email);
		                	   Session::put('phone',$request->phone);
		                	   Session::put('interest',$request->interest);
		 						Session::put('paypalid',$request->paypal);
		                         Session::put('paypalverify','Please enter valid PayPal Account Email Id.');
		                         return redirect('/profileedit');   
	                     }
                 	}
                 	else
                 	{
                 			User::where('_id','=',Auth::id())->update(['name'=>$request->uname,'age' => $request->age,'email' => $request->email,'phone'=>$request->phone,'interest'=>$request->interest]);  	
                 	}

                 }
                else
                { 

                		User::where('_id','=',Auth::id())->update(['name'=>$request->uname,'age' => $request->age,'email' => $request->email,'phone'=>$request->phone,'interest'=>$request->interest]);   
                }
             }
          
                return redirect('/profileedit');   
        
    }
    public function userpaypal($email)
    {
           $ch = curl_init();

          $ppUserID = "k_r_ramya-facilitator_api1.yahoo.com"; 
          $ppPass = "FY56YRRMVUEVT6A7"; 
          $ppSign = "AoS3OUs534Lmv9UKJUzqaeYZAia5A8d262a2yDxTDeSbizUDfir009QB" ;
          $ppAppID = "APP-80W284485P519543T"; //if it is sandbox then app id is always: APP-80W284485P519543T
          $sandboxEmail = "k_r_ramya-facilitator@yahoo.com"; //comment this line if you want to use it in production mode.It is just for sandbox mode

          $emailAddress = $email; //The email address you wana verify

          //parameters of requests
          $nvpStr = 'emailAddress='.$emailAddress.'&matchCriteria=NONE';

          // RequestEnvelope fields
          $detailLevel    = urlencode("ReturnAll");
          $errorLanguage  = urlencode("en_US");
          $nvpreq = "requestEnvelope.errorLanguage=$errorLanguage&requestEnvelope.detailLevel=$detailLevel&";
          $nvpreq .= "&$nvpStr";
          curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

          $headerArray = array(
          "X-PAYPAL-SECURITY-USERID:$ppUserID",
          "X-PAYPAL-SECURITY-PASSWORD:$ppPass",
          "X-PAYPAL-SECURITY-SIGNATURE:$ppSign",
          "X-PAYPAL-REQUEST-DATA-FORMAT:NV",
          "X-PAYPAL-RESPONSE-DATA-FORMAT:JSON",
          "X-PAYPAL-APPLICATION-ID:$ppAppID",
          "X-PAYPAL-SANDBOX-EMAIL-ADDRESS:$sandboxEmail" //comment this line in production mode. IT IS JUST FOR SANDBOX TEST 
          );

          $url="https://svcs.sandbox.paypal.com/AdaptiveAccounts/GetVerifiedStatus";
          curl_setopt($ch, CURLOPT_URL,$url);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_VERBOSE, 1);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
          $paypalResponse = (string)curl_exec($ch);
          //echo $paypalResponse;   //if you want to see whole PayPal response then uncomment it.
          curl_close($ch);
         
           $data = json_decode($paypalResponse);
           $env=$data->responseEnvelope;

          // print_r($paypalResponse);
          return $env->ack;
    }
    

   
}
