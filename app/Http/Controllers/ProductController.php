<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\products_image;
use App\products_draft;
use App\products_draft_image;
use App\subcategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Image;
use Session;    
use App\Events\FollowPublish;
use App\User;
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   public function index()
    {
       
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       if(isset($_GET['id']))
       {
            $proddetails=  products_draft::where('_id', '=',$_GET['id'])->get();
            
            $prodimage=  products_draft_image::where('products_draft_id', '=', $_GET['id'])->get(); 
        
                          
        
       }
       else
       {
        
            $id = Auth::id();
           
       }
           $subcategory=subcategory::all();
      
        return view('products.create',compact('subcategory','proddetails','prodimage'));
 
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            if ($request->typesubmit==0)
            {
                        if ($request->hdndraftid==0) 
                         { 
                           $products= products_draft::create([
                                'title'=>$request->title,
                                'description'=>$request->description,
                                'price'=>$request->price,
                                'status'=>'draft',
                                'category'=>$request->hdncat,
                                'user_id'=>Auth::id(),
                            ]
                            );

                           if ($request->hdnCountry!="")
                           {
                           
                            $id = Auth::id();
                                    
                                  User::where('_id', $id)->update(['lat' =>$request->hdnlat,'long' => $request->hdnlong,'district' =>$request->hdncity,'town' => $request->hdntown,'state' => $request->hdnstate,'country' => $request->hdnCountry]);
                                   $user=User::where('_id', '=', \Auth::user()->id)->first();
                                  
             $currency=config('unit.currency.'.$request->hdnCountry);
                                $distance=config('unit.distance.'.$request->hdnCountry);
                                    $products->update(['currency'=>$currency,'distance'=>$distance]);
                                       Session::put('long',$user->long);
                                     Session::put('lat',$user->lat);
                                       Session::put('town', $user->town);
                                    
                                       Session::put('district', $user->district);
                                    
                                       Session::put('state',$user->state);
                                    
                                       Session::put('country', $user->country);
                                }
                                else
                                   if (!empty(Session::get('country')))
                                   {

                                    $currency=config('unit.currency.'.Session::get('country'));
                                   
                                   
                                $distance=config('unit.distance.'.Session::get('country'));
                                    $products->update(['currency'=>$currency,'distance'=>$distance]);
                                   }

                           $intdraftid=$products->id;
                        }
                        else
                        {
                                $products= products_draft::where('_id','=',$request->hdndraftid)->update([
                                'title'=>$request->title,
                                'description'=>$request->description,
                                'price'=>$request->price,
                                'status'=>'draft',
                                'category'=>$request->hdncat,
                                
                            ]); 
                                $intdraftid=$request->hdndraftid;
                        }
                       
                      $intPosition=1;
                      if (!empty(Session::get('imgUrl')))
                      {
                         products_draft_image::where('products_draft_id',$intdraftid)->delete();
                        foreach (Session::get('imgUrl') as $item1)
                        {
                                $image = Image::make($item1);
                                 $filename=Auth::id().$request->title. $intPosition.'.jpg';
                                $image->save(storage_path().'/app/public/photos/'.$filename);
   
                                 $filename1='photos/'.Auth::id().$request->title.$intPosition.'.jpg';
                                    products_draft_image::create([
                                    'products_draft_id' =>$intdraftid,
                                    'filename' => $filename1,
                                    'position'=>$intPosition,
                                          ]);          
                        $intPosition= $intPosition+1;
                        }
                    }
                          Session::forget('draftid', "");
                          Session::forget('title', "");
                             Session::forget('description',"");
                             Session::forget('price', "");
                             Session::forget('category', "");   
                             Session::forget('imgUrl',"");
                        
              
              return redirect()->route('publish',['id' => $intdraftid])
                        ->with('success','Product created successfully.');
            }
            elseif ($request->typesubmit==1)
            {
                 if ($request->hdndraftid!=0) 
                         { 
                              Session::put('draftid',$request->hdndraftid);
                          }
                Session::put('title', $request->title);
                 Session::put('description', $request->description);
                 Session::put('price', $request->price);
                 Session::put('category', $request->hdncat);
                
                  if ($request->hdndraftid!=0) 
                  { 
                     $prodimage=  products_draft_image::where('products_draft_id', '=', $request->hdndraftid)->get();

                          return view('products.capture',compact('prodimage'));
                   }
                else
                 {
                        return view('products.capture');
                        }
            }
        elseif ($request->typesubmit==2)
            {
                 if ($request->hdndraftid!=0) 
                   { 
                   
                    $prod= products::where('draft_id',$request->hdndraftid)->first();
                    if(!empty($prod))
                    {
                    products_image::where('products_id',$prod->_id)->delete();
                    products::where('_id',$prod->_id)->delete();
                }
                     products_draft_image::where('products_draft_id',$request->hdndraftid)->delete();
                      products_draft::where('_id',$request->hdndraftid)->delete();
                   // print($prod->_id);
                   }
                    return redirect('/home');    
            }
                            
      
                        
     }
                    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Load Publish Page
     public function publish($id)
    {
           $proddetails=  products_draft::where('_id', '=',$id)->get();
        $prodimage=  products_draft_image::where('products_draft_id', '=', $id)->get(); 
          $subcategory=subcategory::all();

        return view('products.publish',['id' => $id],compact('proddetails','prodimage','subcategory'));

    }
   public function publishstore(Request $request)
    {
           /*  Session::forget('title', "");
                 Session::forget('description',"");
                 Session::forget('price', "");
                 Session::forget('category', "");   
                 Session::forget('imgUrl',""); */
                if ($request->hdnproductid!="0")
                 {
                   $intproductid=$request->hdnproductid;
                  $products= products::where('_id', '=',$intproductid)->update([
                            'title'=>$request->title,
                            'description'=>$request->description,
                            'price'=>$request->price,
                            'status'=>'published',
                            'category'=>$request->hdncat,
                           
                        ]
                        );
                  products_image::where('products_id',$intproductid)->delete();
                      
               products_draft::where('product_id','=',$intproductid)->update(['status' =>'published']);   
              }
              else
              {
                       $user=User::where('_id', '=', \Auth::user()->id)->first();

                            if ($user->paypalverified!="1")
                              {
                                 //$paypalexist= $this->userpaypal($user->email);
                             
                                          /*   if ($paypalexist=='Success')
                                           {
                                              $payuser= User::where('_id','=',\Auth::user()->id)->update([
                                                                  
                                                                  'paypalverified'=>1,
                                                                  
                                                              ]); 
                                           }*/
                                       if ($request->paypal!="" )
                                       {
                                              $paypalexist= $this->userpaypal($request->paypal);
                                             
                                                if ($paypalexist=='Success')
                                             {
                                                $payuser= User::where('_id','=',\Auth::user()->id)->update([
                                                                    
                                                                    'paypalverified'=>1,
                                                                    'paypalid'=>$request->paypal
                                                                ]); 
                                             }
                                             else
                                             {
                                                  Session::put('paypalverify','Please enter valid PayPal Email Id.');
                                                  Session::put('paypalid',$request->paypal);
                                                 $proddetails=  products_draft::where('_id', '=',$request->hdnid)->get();
                                                  $prodimage=  products_draft_image::where('products_draft_id', '=', $request->hdnid)->get(); 
                                                    $subcategory=subcategory::all();

                                                  return view('products.publish',['id' =>$request->hdnid],compact('proddetails','prodimage','subcategory'));   
                                             }
                                       }
                                       else
                                       {
                                               Session::put('paypalverify','Please note a PayPal account is required to publish your product.');
                                                 $proddetails=  products_draft::where('_id', '=',$request->hdnid)->get();
                                                  $prodimage=  products_draft_image::where('products_draft_id', '=', $request->hdnid)->get(); 
                                                    $subcategory=subcategory::all();

                                                  return view('products.publish',['id' =>$request->hdnid],compact('proddetails','prodimage','subcategory'));
                                        }
                                   }

                    $products= products::create([
                            'title'=>$request->title,
                            'description'=>$request->description,
                            'price'=>$request->price,
                            'status'=>'published',
                            'category'=>$request->hdncat,
                            'user_id'=>Auth::id(),
                            'draft_id'=>$request->hdnid,
                            'city'=>Session::get('district'),
                        ]
                        );

                     
                     $intproductid=$products->id;
                   
               products_draft::where('_id','=',$request->hdnid)->update(['product_id' =>  $products->_id,'status' =>'published']); 
               $draft=products_draft::where('_id','=',$request->hdnid)->first();
              
                                    $products->update(['currency'=>$draft->currency,'distance'=>$draft->distance]);
//event(new PostPublish($products));
//event(new PostPublish($products));
              }
              $prodimage=  products_draft_image::where('products_draft_id', '=', $request->hdnid)->get(); 
                   $intPosition=1;
                        
                            foreach ($prodimage as $item1)
                            {
                                   
       
                             $filename1='photos/'.Auth::id().$request->title.$intPosition.'.jpg';
                                products_image::create([
                                'products_id' =>$intproductid,
                                'filename' => $item1->filename,
                                'position'=>$item1->position,
                                      ]);          
                            $intPosition= $intPosition+1;
                            }
                      $proddet=products::where('_id', '=', $intproductid)->get();
                      $prodimages=products_image::where('products_id', '=', $intproductid)->get();
                return view('products.publishsuccess',compact('proddet','prodimages'));     
     }  
     public function publishsuccess(Request $request)
    {
         
              // return redirect('/products/create');  

     }                 
 public function capturestore(Request $request)
    {
          $strimg=ltrim($request->hdnSnap,"@@");

          $arrimg=explode('@@',$strimg);
            Session::put('imgUrl',$arrimg);

             if(Session()->has('draftid') )
                 return redirect('/products/create?id='.Session('draftid'));   
             else
               return redirect('/products/create');     
     }  
    public function userpaypal($email)
    {
           $ch = curl_init();

          $ppUserID = "k_r_ramya-facilitator_api1.yahoo.com"; 
          $ppPass = "FY56YRRMVUEVT6A7"; 
          $ppSign = "AoS3OUs534Lmv9UKJUzqaeYZAia5A8d262a2yDxTDeSbizUDfir009QB" ;
          $ppAppID = "APP-80W284485P519543T"; //if it is sandbox then app id is always: APP-80W284485P519543T
          $sandboxEmail = "k_r_ramya-facilitator@yahoo.com"; //comment this line if you want to use it in production mode.It is just for sandbox mode

          $emailAddress = $email; //The email address you wana verify

          //parameters of requests
          $nvpStr = 'emailAddress='.$emailAddress.'&matchCriteria=NONE';

          // RequestEnvelope fields
          $detailLevel    = urlencode("ReturnAll");
          $errorLanguage  = urlencode("en_US");
          $nvpreq = "requestEnvelope.errorLanguage=$errorLanguage&requestEnvelope.detailLevel=$detailLevel&";
          $nvpreq .= "&$nvpStr";
          curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

          $headerArray = array(
          "X-PAYPAL-SECURITY-USERID:$ppUserID",
          "X-PAYPAL-SECURITY-PASSWORD:$ppPass",
          "X-PAYPAL-SECURITY-SIGNATURE:$ppSign",
          "X-PAYPAL-REQUEST-DATA-FORMAT:NV",
          "X-PAYPAL-RESPONSE-DATA-FORMAT:JSON",
          "X-PAYPAL-APPLICATION-ID:$ppAppID",
          "X-PAYPAL-SANDBOX-EMAIL-ADDRESS:$sandboxEmail" //comment this line in production mode. IT IS JUST FOR SANDBOX TEST 
          );

          $url="https://svcs.sandbox.paypal.com/AdaptiveAccounts/GetVerifiedStatus";
          curl_setopt($ch, CURLOPT_URL,$url);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_VERBOSE, 1);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
          $paypalResponse = (string)curl_exec($ch);
          //echo $paypalResponse;   //if you want to see whole PayPal response then uncomment it.
          curl_close($ch);
         
           $data = json_decode($paypalResponse);
           $env=$data->responseEnvelope;

          // print_r($paypalResponse);
          return $env->ack;
    }
}

