<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\products_image;
use App\subcategory;
use App\User;
use App\Follow;
use App\book_product;
use App\category;

class AdminController extends Controller
{
    //
    public function index()
    {
    	$category=category::all();
      $subcategory=subcategory::all();
      $location = array('chennai' =>'Chennai' ,'Mumbai'=>'Mumbai', 'Delhi'=>'Delhi', 'Bengaluru'=>'Bengaluru');
      $booking=  book_product::all(); 
       $location = User::raw()->distinct('district');
        $user = User::all();
        $renter=User::all();
               $bookings=null;
               $product=products::all();
      if(isset($_GET['category']))
        {



	          if ($_GET['category']!='0')
	          { 

				         if(isset($_GET['location']) && $_GET['location']!='0')
				        {
				         	
				         $prods=  products::where('category', '=', $_GET['category'])->where('city','=',$_GET['location'])->get();
				        }
				        else
				        {
				          	$prods=  products::where('category', '=', $_GET['category'])->get();
				         }

				          	$strids='';
				                     $i=0;
				                      foreach ($prods as $prod) {
				                        $strids=$strids.','.$prod->_id.'';
				                       

				                      }

				                      if ($strids!='')
				                      {
				                        
				                        	$strids=ltrim($strids,",");
				                   			$arraystr = explode(',', $strids);
				                   			 if ($_GET['tstart']!='null')
	          								{ 
	          									$booking= book_product::whereIn('product_id', ($arraystr))->where('start_dt','>=',$_GET['tstart'])->where('end_dt','<=',$_GET['tend'])->get();
	          								}
	          								else
				                           $booking= book_product::whereIn('product_id', ($arraystr))->get();
	                  
	             					}
	       

	          }
	          elseif ($_GET['location']!='0')
	          {
	          					$prods=  products::where('city','=',$_GET['location'])->get();
				   				$strids='';
				                     $i=0;
				                      foreach ($prods as $prod) {
				                        $strids=$strids.','.$prod->_id.'';
				                       

				                      }

				                      if ($strids!='')
				                      {
				                        
				                        	$strids=ltrim($strids,",");
				                   			$arraystr = explode(',', $strids);
				                   			 if ($_GET['tstart']!='null')
	          								{ 
	          									$booking= book_product::whereIn('product_id', ($arraystr))->where('start_dt','>=',$_GET['tstart'])->where('end_dt','<=',$_GET['tend'])->get();
	          								}
	          								else
				                           $booking= book_product::whereIn('product_id', ($arraystr))->get();
	                  
	             					}
	          }
	          elseif ($_GET['tstart']!='null')
	          {
	          		$booking= book_product::where('start_dt','>=',$_GET['tstart'])->where('end_dt','<=',$_GET['tend'])->get();
	          }
         
        }
        
        
        

        return view('admin.basicreport',compact('category','subcategory','location','booking','user','renter','product'));
    }
}
