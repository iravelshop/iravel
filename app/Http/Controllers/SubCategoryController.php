<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\subcategory;
use App\category;


class SubCategoryController extends Controller
{
    
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	 $category=category::all();
       
       
        return view('subcategory.create',compact('category'));
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
        $filename='';

        if ($request->hasFile('catphoto'))
                {
        $image=$request->catphoto;
 			$filename = $image->store('admin\subcategory');
				}
         $subcategory= subcategory::create([
                            'name'=>$request->name,
                            'description'=>$request->description,
                           'category_id'=>$request->category,
                            'status'=>'active'
                        ]
                        );

       // cate::create($request->all());
   
        return redirect()->route('subcategory.create')
                        ->with('success','Product created successfully.');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('subcategory.show',compact('product'));
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('subcategory.edit',compact('product'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, category $product)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
  
      //  $product->update($request->all());
  
        return redirect()->route('category.index')
                        ->with('success','Product updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
      //  $product->delete();
  
        return redirect()->route('category.index')
                        ->with('success','Product deleted successfully');
    }

   
}
