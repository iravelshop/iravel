<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Review;
use Illuminate\Http\Request;
use Session;
class FeedBackController extends Controller
{
   


   public function storeFeedBack(Request $request)
   {
    if ($request->hdnreview==0)
    {
       $review= review::create([
                            'bookingid'=>$request->hdnbookid,
                            'rating'=>$request->hdnRating,
                            'comment'=>$request->comment,
                            'productid'=>$request->hdnprodid,
                            'renter'=>Auth::id()
                         ]
                        );
       Session::put('Feedsave','Feed Back Saved');
     }
     else
     {
        $review= review::where('_id', '=', $request->hdnreview)->first();
                        
                             $review->rating=$request->hdnRating;
                             $review->comment=$request->comment;
                            
                $review->save();
                Session::put('Feedsave','Feed Back Saved');
     }
       return redirect('mybookings?bookingid='.$request->hdnbookid);
     
   }
}