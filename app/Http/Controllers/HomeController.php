<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\products;
use App\products_image;
use App\subcategory;
use Jenssegers\Mongodb\Eloquent\Model;
use App\Http\Controllers\DB;
use App\User;
use App\Message;
use App\Http\Controllers\Auth;
use Session;  
use Log;
class HomeController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      Log::info('home page before- test');
        $this->middleware('auth');
        Log::info('home page after- test');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(\Request $request)
    {
       
       Session::forget('title', "");
                             Session::forget('description',"");
                             Session::forget('price', "");
                             Session::forget('category', "");   
                             Session::forget('imgUrl',"");
        $category=category::all();
      $subcategory=subcategory::all();
       $user=User::where('_id', '=', \Auth::user()->id)->first();
       
        if(isset($user->lat))
       Session::put('lat', $user->lat);
     if(isset($user->long))
       Session::put('long', $user->long);
     if(isset($user->town))
       Session::put('town', $user->town);
     if(isset($user->district))
       Session::put('district', $user->district);
     if(isset($user->state))
       Session::put('state', $user->state);
     if(isset($user->country))
       Session::put('country', $user->country);
     //$city = User::orderBy('district','asc')->raw()->distinct('district') ;
     $city = User::orderBy('district','asc')->raw()->distinct('district') ;
      if(!empty(Session::get('district')))
    $userCity= Session::get('district');
  else
    $userCity='';
  
  
        if(isset($_GET['category']))
                {
                    
      
                     $subcategory=subcategory::where('category_id', '=', $_GET['category'])->get();
                   
                     $strids='';
                     $i=0;
                      foreach ($subcategory as $sub) {
                        $strids=$strids.','.$sub->_id.'';
                       

                      }
                    
                      if ($strids!='')
                      {
                        
                        $strids=ltrim($strids,",");
                       
                        $arraystr = explode(',', $strids);
                        if(isset($_GET['city']))
                            $products= products::whereIn('category', ($arraystr))->where('city',$_GET['city'])->get();
                        else
                        {   if($userCity=='')
                              $products= products::whereIn('category', ($arraystr))->get();
                          else
                              $products= products::whereIn('category', ($arraystr))->where('city',Session::get('district'))->get();
                          }
                          $products_image = products_image::where('position', '=', 1)->get();
                    
                  }
       
                }
                else
                 if(isset($_GET['subcategory']))
                  {
                    
                        if(isset($_GET['city']))
                           $products = products::where('category', '=', $_GET['subcategory'])->where('city',$_GET['city'])->get();
                        else
                          {   if($userCity=='')
                              $products = products::where('category', '=', $_GET['subcategory'])->get();
                              else
                                 $products = products::where('category', '=', $_GET['subcategory'])->where('city',Session::get('district'))->get();
                            }
                        $products_image = products_image::where('position', '=', 1)->get();
                       // $products_image = products_image::get();
                      
                   
                    }
                    elseif(isset($_GET['search']))
                  {
                   
                   //   $products = products::where('name','=','/'. $_GET['search'].'/')->get();
                       $products = products::where('title','regexp','/.*'.$_GET['search'].'/i')->get();
                       $products_image = products_image::where('position', '=', 1)->get();
                  }
                  else
                    {
                          $subcategory=subcategory::all();

                          if(isset($_GET['city']))
                          {
                         
                               $products = products::where('status','published')->where('city',$_GET['city'])->get();  

                                 if(isset($_GET['lat']))
                                 {
                                   $id = \Auth::user()->id;
                                     $user->save();
                                  User::where('_id', $id)->update(['lat' => $_GET['lat'],'long' => $_GET['long'],'district' => $_GET['city'],'town' => $_GET['town'],'state' => $_GET['state'],'country' => $_GET['ctry']]);
                                  Session::put('lat', $user->lat);
     
                                       Session::put('long',$_GET['long']);
                                     Session::put('lat',$_GET['lat']);
                                       Session::put('town', $_GET['town']);
                                    
                                       Session::put('district', $_GET['city']);
                                    
                                       Session::put('state',$_GET['state']);
                                    
                                       Session::put('country', $_GET['ctry']);

                                 }
                          }
                          else
                          {

                            //if($userCity=='')
                               $products = products::where('status','published')->get(); 
                            /* else
                              $products = products::where('status','published')->where('city',Session::get('district'))->get();*/
                         } 
                          $products_image = products_image::where('position', '<', 2)->get();
                    }
                 
                         
        return view('homepage',compact('products','category','subcategory','products_image','city') );
        
    }
     public function private()
    {
      $id = \Auth::user()->id;
    
      $users=User::where('_id', '<>',$id)->get();

      $messages=Message::with(['sender','receiver'])
                ->where(function ($query) use ($id){
                $query->where('user_id', '=', $id)
                      ->where('latestchat', '=', 'y');
            })
           ->orWhere(function ($query) use($id) {
                $query->where('latestchat', '=', 'y')
                      ->where('receiver_id', '=', $id);
            })
         ->orderBy('created_at','desc')->get(['latestchat','message','user_id','receiver_id','created_at']);
   
      // $messages1 = Message::groupBy('reciever_id','message')->get();
 /*foreach ($messages as $msg) {
                        print_r($msg->created_at);
                         print_r($msg->message);

                      }
          exit;   */       

        $messageuser = Message::where('receiver_id','=',$id)->pluck('user_id')->all();
         $messagereceiver = Message::where('user_id','=',$id)->pluck('receiver_id')->all();
        
$userrest = User::whereNotIn('_id', $messageuser)->whereNotIn('_id', $messagereceiver)->where('_id', '<>',$id)->get();

        return view('private',compact('messages','users','userrest'));
    }
     public function privatechat()
    {
       if(isset($_GET['userid']))
                  {
                     $actusers= User::where('_id',$_GET['userid'])->get();
                   }
        return view('private-chat',compact('actusers'));
    }
      public function search()
    {
      if(isset($_GET['text']))
      {
        $products = products::where('title','regexp','/.*'.$_GET['text'].'/i')->get();
                       $products_image = products_image::where('position', '=', 1)->get();
                       return view('search',compact('products','products_image'));
      }
      else
        return view('search');
    }
     public function users()
    {
         if(isset($_GET['userid']))
                  {
                     return User::where('_id',$_GET['userid'])->get();
                   }
                   else
                    return User::All();
    }
}
