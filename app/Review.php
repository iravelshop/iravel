<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model;

class Review extends Model
{
	 protected $table = 'review';
     protected $fillable = [
      'bookingid',
      'owner',
      'renter',
      'comment',
      'rating','productid'
    
    ];
 
}
