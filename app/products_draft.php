<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model;

class products_draft extends Model
{
   protected $table = 'products_draft';

   
      protected $fillable = [
      'title',
      'description',
      'price','category','status','user_id','currency','distance'
    ];
    public function productsdraftimage()
    {
        return $this->hasMany('products_draft_image');
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
