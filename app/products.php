<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model;

class products extends Model
{
   protected $table = 'products';

   
      protected $fillable = [
      'title',
      'description',
      'price','category','status','user_id','draft_id','city','currency','distance'
    ];
    public function productsimage()
    {
        return $this->hasMany('products_image');
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
