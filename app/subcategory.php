<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class subcategory extends Model
{
    protected $fillable = [
      'name',
      'description',
      'status',
     'category_id'
    ];
 public function category()
    {
       return $this->belongsTo(category::class);
    }
}
