<?php

// FollowProduct.php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use App\Product;
class FollowProduct implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   public $owner;

public $product;
    /**
     * Create a new event instance.
     *
     * @return void
     */
   public function __construct(User $owner,Product $product )
    {
        $this->owner = $owner;
        $this->product=$product;
        
      
    }

  

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
         $follow=follow::where('follow_id','=',$this->user->id)->get();
         foreach ($follow as $follower) 
         {
                return new Channel('follow.'.$follower->user_id);
            }
    }
}