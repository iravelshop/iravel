<?php

// PostCreated.php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use App\book_product;
class PostPublish implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   public $user;
public $renter;
public $booking;
    /**
     * Create a new event instance.
     *
     * @return void
     */
   public function __construct(User $user,book_product $booking,User $renter )
    {
        $this->user = $user;
        $this->renter=$renter;
        $this->booking=$booking;
      
    }

  

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {

        return new Channel('booking.'.$this->user->id);
    }
}