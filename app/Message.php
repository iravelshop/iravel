<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;


class Message extends Model
{
    protected $guarded=[];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function sender()
	{
	    return $this->belongsTo(User::class, 'user_id');
	}


	public function receiver()
	{
	    return $this->belongsTo(User::class, 'receiver_id');
	}
}
