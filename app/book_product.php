<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class book_product extends Model
{
     protected $table = 'book_product';
    protected $fillable = ['title',
      'product_id',
      'start_dt',
      'end_dt','user_id',
                            'noofdays',
                            'total','payment','transactionid'
      
    ];
}
