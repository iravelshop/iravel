<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model;

class Invoice extends Model
{
	 protected $table = 'invoice';
     protected $fillable = [
      'paymentid',
      'bookingid',
      'status',
    
    ];
 
}
