<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class category extends Model
{
	 protected $table = 'category';
    protected $fillable = [
      'name',
      'description',
      'status','catimage'
      
    ];

}
