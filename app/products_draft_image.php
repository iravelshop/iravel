<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class products_draft_image extends Model
{
    protected $table = 'products_draft_image';
    protected $fillable = ['products_draft_id', 'filename','position'];
     
}
