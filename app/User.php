<?php

namespace App;

use Illuminate\Notifications\Notifiable;

use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
   const ADMIN_TYPE = 'admin';
const DEFAULT_TYPE = 'default';
public function isAdmin()    {        
    return $this->type === self::ADMIN_TYPE;    
}

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone','age','password','lat','long','town','district','state','country','google_id','type','paypalverified','verified','paypalid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
    public function messagesSent() 
{
    return $this->hasMany(User::class, 'user_id');
}


/**
 * Messages that were sent to the user
 */
public function messagesReceived() 
{
    return $this->hasMany(User::class, 'received_id');
}
public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }
}
