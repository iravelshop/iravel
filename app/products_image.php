<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class products_image extends Model
{
    protected $table = 'products_image';
    protected $fillable = ['products_id', 'filename','position'];
     
}
