<?php

return [
    'name' => 'Iravel Shop',
    'manifest' => [
        'name' => env('APP_NAME', 'Iravel Shop'),
        'short_name' => 'Iravel',
        'start_url' => 'https://iravelshop.com',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation'=> 'any',
        'icons' => [
            '72x72' => '/svg/icons/icon-72x72.png',
            '96x96' => '/svg/icons/icon-96x96.png',
            '128x128' => '/svg/icons/icon-128x128.png',
            '144x144' => '/svg/icons/icon-144x144.png',
            '152x152' => '/svg/icons/icon-152x152.png',
            '192x192' => '/svg/icons/icon-192x192.png',
            '384x384' => '/svg/icons/icon-384x384.png',
            '512x512' => '/svg/icons/icon-512x512.png'
        ],
        'splash' => [
           '640x1136' => '/svg/icons/splash-640x1136.png',
            '750x1334' => '/svg/icons/splash-750x1334.png',
            '828x1792' => '/svg/icons/splash-828x1792.png',
            '1125x2436' => '/svg/icons/splash-1125x2436.png',
            '1242x2208' => '/svg/icons/splash-1242x2208.png',
            '1242x2688' => '/svg/icons/splash-1242x2688.png',
            '1536x2048' => '/svg/icons/splash-1536x2048.png',
            '1668x2224' => '/svg/icons/splash-1668x2224.png',
            '1668x2388' => '/svg/icons/splash-1668x2388.png',
            '2048x2732' => '/svg/icons/splash-2048x2732.png',
        ],
        'custom' => []
    ]
];
