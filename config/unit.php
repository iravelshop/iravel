<?php

return array(
    'currency' => array(
           'India' => '₹',
           'United States' =>'$',
    ),
    'distance' => array(
           'India' => 'Kms',
           'United States' =>'miles',
    ),
);