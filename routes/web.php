<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

if(!isset($_COOKIE['firsttimer'])){
    // if cookie ain't set, show link
     setcookie('firsttimer','something',strtotime('+1 year'),'/');
    $_COOKIE['firsttimer']='something'; // cookie is delayed, so we do this fix
    return view('intro');
}else{
	if(Auth::check())
	return redirect()->route('home');
	else
        return redirect()->route('login');
  // return view('welcome');
}
 
});

Route::get('/welcome', function () {
   return redirect()->route('login');
});
Route::get('/details', 'DetailsController@index');
// route for processing payment
Route::post('paypal', 'DetailsController@payWithpaypal')->name('paypal');
Route::get('/getPaymentStatus', 'DetailsController@getPaymentStatus')->name('getPaymentStatus');


Auth::routes();


Route::get('logout', 'Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/private', 'HomeController@private')->name('private');
Route::get('/privatechat', 'HomeController@privatechat')->name('privatechat');
Route::get('/privatechatall', 'HomeController@privatechatall')->name('privatechatall');
Route::get('/users', 'HomeController@users')->name('users');
Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');
Route::get('/iphone', 'GeoLocationController@currentlocation');
//Route::get('/iphone1', 'GeoLocationController@iphone');
Route::get('/iphone2', 'GeoLocationController@iphone2');

Route::get('/bookings', 'DetailsController@userbookings')->name('bookings');
Route::get('/mybookings', 'DetailsController@mybookings')->name('mybookings');
Route::get('messages', 'MessageController@fetchMessages');
Route::post('messages', 'MessageController@sendMessage');
Route::get('/last-messages/{user}', 'MessageController@LastMessages')->name('LastMessages');
Route::get('/private-messages/{user}', 'MessageController@privateMessages')->name('privateMessages');
Route::post('/private-messages/{user}', 'MessageController@sendPrivateMessage')->name('privateMessages.store');
Route::resource('/products','ProductController');
Route::get('/publish/{id}', 'ProductController@publish')->name('publish');
Route::post('/publish', 'ProductController@publishstore')->name('publishstore');

Route::resource('/profileedit','ProfileController');
Route::get('/profile','ProfileController@profile')->name('profile');

Route::get('/capture', 'ProductController@capture')->name('capture');
Route::post('/capture', 'ProductController@capturestore')->name('capturestore');
Route::post('/publishsuccess', 'ProductController@publishsuccess')->name('publishsuccess');
//Route::post('paypal', 'PaymentController@payWithpaypal');
//Route::get('/status', 'PaymentController@getPaymentStatus')->name('status');

Route::get('/paymentthanks', 'PaymentController@paymentthanks')->name('paymentthanks');
Route::get('/rating', 'DetailsController@rating')->name('rating');


//Route::post('products/upload', 'ProductController@store')->name('products.upload');
//Route::get('/multiuploads', 'ProductController@uploadForm');
Route::resource('category','CategoryController');
Route::get('/admin', 'AdminController@index')    
    ->middleware('is_admin')    
    ->name('admin');
Route::resource('subcategory','SubCategoryController');
//Route::resource('details','DetailsController');
//payment form

// route for check status of the payment

Route::get('test', function () {
    event(new App\Events\PostPublish('Someone'));
    return "Event has been sent!";
});
Route::get('/notify', 'PusherNotificationController@sendNotification');
Route::get('/follow', 'FollowController@follow');
Route::post('/reset', 'LoginController@reset')->name('reset');

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::post('/feedstore', 'FeedBackController@storeFeedBack')->name('feedstore');